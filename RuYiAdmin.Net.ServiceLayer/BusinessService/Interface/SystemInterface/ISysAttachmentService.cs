﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BaseService.Interface;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface
{
    /// <summary>
    /// 业务附件服务层接口
    /// </summary>
    public interface ISysAttachmentService : IRuYiAdminBaseService<SysAttachment>
    {
        /// <summary>
        /// 系统文件统计
        /// </summary>
        /// <returns>ActionResponseResult</returns>
        Task<ActionResponseResult> QuerySysFileStatisticalInfo();
    }
}
