﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BaseService.Interface;
using System;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface
{
    /// <summary>
    /// 计划任务业务层接口
    /// </summary>
    public interface ISysScheduleJobService : IRuYiAdminBaseService<SysScheduleJob>
    {
        /// <summary>
        /// 启动计划任务
        /// </summary>
        /// <param name="jobId">任务编号</param>
        /// <param name="userId">操作人员编号</param>
        /// <returns></returns>
        Task StartScheduleJobAsync(Guid jobId, Guid userId);

        /// <summary>
        /// 暂停计划任务
        /// </summary>
        /// <param name="jobId">作业编号</param>
        /// <param name="userId">操作人员编号</param>
        /// <returns></returns>
        Task PauseScheduleJobAsync(Guid jobId, Guid userId);

        /// <summary>
        /// 恢复计划任务
        /// </summary>
        /// <param name="jobId">作业编号</param>
        /// <param name="userId">操作人员编号</param>
        /// <returns></returns>
        Task ResumeScheduleJobAsync(Guid jobId, Guid userId);

        /// <summary>
        /// 删除计划任务
        /// </summary>
        /// <param name="jobId">作业编号</param>
        /// <param name="userId">操作人员编号</param>
        /// <returns></returns>
        Task DeleteScheduleJobAsync(Guid jobId, Guid userId);

        /// <summary>
        /// 启动业务作业
        /// </summary>
        /// <returns></returns>
        Task StartScheduleJobAsync();

        /// <summary>
        /// 加载计划任务缓存
        /// </summary>
        Task LoadBusinessScheduleJobCache();

        /// <summary>
        /// 清理计划任务缓存
        /// </summary>
        Task ClearBusinessScheduleJobCache();
    }
}
