﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BaseService.Interface;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface
{
    /// <summary>
    /// 审计日志业务层接口
    /// </summary>
    public interface ISysLogService : IRuYiAdminBaseService<SysLog>
    {
    }
}
