﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BaseService.Interface;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface
{
    /// <summary>
    /// 角色业务层接口
    /// </summary>
    public interface ISysRoleService : IRuYiAdminBaseService<SysRole>
    {
        /// <summary>
        /// 加载系统角色缓存
        /// </summary>
        Task LoadSystemRoleCache();

        /// <summary>
        /// 清理系统角色缓存
        /// </summary>
        Task ClearSystemRoleCache();
    }
}
