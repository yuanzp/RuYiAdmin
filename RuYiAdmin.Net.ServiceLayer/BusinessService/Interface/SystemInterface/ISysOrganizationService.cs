﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BaseService.Interface;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface
{
    /// <summary>
    /// 机构业务层接口
    /// </summary>
    public interface ISysOrganizationService : IRuYiAdminBaseService<SysOrganization>
    {
        /// <summary>
        /// 获取机构树
        /// </summary>
        /// <returns>ActionResponseResult</returns>
        Task<QueryResponseResult<SysOrganizationDTO>> GetOrgTreeNodes();

        /// <summary>
        /// 获取机构、用户树
        /// </summary>
        /// <returns></returns>
        Task<QueryResponseResult<OrgUserTreeDTO>> GetOrgUserTree();

        /// <summary>
        /// 加载系统机构缓存
        /// </summary>
        Task LoadSystemOrgCache();

        /// <summary>
        /// 清理系统机构缓存
        /// </summary>
        Task ClearSystemOrgCache();
    }
}
