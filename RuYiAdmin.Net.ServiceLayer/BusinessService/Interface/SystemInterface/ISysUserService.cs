﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BaseService.Interface;
using System;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface
{
    /// <summary>
    /// 用户业务层接口
    /// </summary>
    public interface ISysUserService : IRuYiAdminBaseService<SysUser>
    {
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="userId">用户编号</param>
        /// <returns>ActionResponseResult</returns>
        Task<ActionResponseResult> DeleteEntity(Guid userId);

        /// <summary>
        /// 批量删除用户
        /// </summary>
        /// <param name="ids">编号组</param>
        /// <returns>ActionResponseResult</returns>
        Task<ActionResponseResult> DeleteEntities(string ids);

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="loginDTO">登录信息</param>
        /// <returns>ActionResponseResult</returns>
        Task<ActionResponseResult> Logon(LoginDTO loginDTO);

        /// <summary>
        /// 退出登录
        /// </summary>
        /// <param name="token">token</param>
        /// <param name="operationType">退出类型</param>
        /// <param name="remark">说明信息</param>
        /// <returns>ActionResponseResult</returns>
        Task<ActionResponseResult> Logout(string token, OperationType operationType = OperationType.Logout, string remark = null);

        /// <summary>
        /// 更新用户密码
        /// </summary>
        /// <param name="data">参数</param>
        /// <returns>ActionResponseResult</returns>
        Task<ActionResponseResult> UpdatePassword(PasswordDTO data);

        /// <summary>
        /// 获取在线用户
        /// </summary>
        /// <returns>QueryResponseResult</returns>
        Task<QueryResponseResult<SysUserDTO>> GetOnlineUsers();

        /// <summary>
        /// 加载系统用户缓存
        /// </summary>
        Task LoadSystemUserCache();

        /// <summary>
        /// 清理系统用户缓存
        /// </summary>
        Task ClearSystemUserCache();
    }
}
