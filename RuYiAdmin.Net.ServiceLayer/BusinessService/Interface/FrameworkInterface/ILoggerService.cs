﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using System;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.FrameworkInterface
{
    /// <summary>
    /// 业务日志服务层接口
    /// </summary>
    public interface ILoggerService
    {
        /// <summary>
        /// 普通日志
        /// </summary>
        /// <param name="message">摘要</param>
        /// <param name="exception">异常</param>
        void Info(string message, Exception exception = null);

        /// <summary>
        /// 警告日志
        /// </summary>
        /// <param name="message">摘要</param>
        /// <param name="exception">异常</param>
        void Warn(string message, Exception exception = null);

        /// <summary>
        /// 错误日志
        /// </summary>
        /// <param name="message">摘要</param>
        /// <param name="exception">异常</param>
        void Error(string message, Exception exception = null);

        /// <summary>
        /// Debug日志
        /// </summary>
        /// <param name="message">摘要</param>
        /// <param name="exception">异常</param>
        void Debug(string message, Exception exception = null);
    }
}
