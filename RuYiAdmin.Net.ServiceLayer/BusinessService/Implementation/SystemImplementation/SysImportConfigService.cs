﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using AutoMapper;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.SystemInterface;
using RuYiAdmin.Net.ServiceLayer.BaseService.Implementation;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;
using System.Collections.Generic;
using System.Linq;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Implementation.SystemImplementation
{
    /// <summary>
    /// 导入配置业务层实现
    /// </summary>
    public class SysImportConfigService : RuYiAdminBaseService<SysImportConfig>, ISysImportConfigService
    {
        #region 属性及构造函数

        /// <summary>
        /// 导入配置访问层实例
        /// </summary>
        private readonly ISysImportConfigRepository ImportConfigRepository;

        /// <summary>
        /// 导入配置明细访问层实例
        /// </summary>
        private readonly ISysImportConfigDetailRepository ImportConfigDetailRepository;

        /// <summary>
        /// AutoMapper实例
        /// </summary>
        private readonly IMapper AutoMapper;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="ImportConfigRepository"></param>
        /// <param name="ImportConfigDetailRepository"></param>
        /// <param name="AutoMapper"></param>
        public SysImportConfigService(ISysImportConfigRepository ImportConfigRepository,
                                      ISysImportConfigDetailRepository ImportConfigDetailRepository,
                                      IMapper AutoMapper) : base(ImportConfigRepository)
        {
            this.ImportConfigRepository = ImportConfigRepository;
            this.ImportConfigDetailRepository = ImportConfigDetailRepository;
            this.AutoMapper = AutoMapper;
        }

        #endregion

        #region 获取导入配置

        /// <summary>
        /// 获取导入配置
        /// </summary>
        /// <param name="configName">配置名称</param>
        /// <returns>配置信息</returns>
        public ImportConfigDTO GetImportConfig(string configName)
        {
            var config = this.ImportConfigRepository.GetList().Where(t => t.IsDel == (int)DeletionType.Undeleted && t.ConfigName.Equals(configName)).FirstOrDefault();

            var configDetails = this.ImportConfigDetailRepository.GetList().Where(t => t.IsDel == (int)DeletionType.Undeleted && t.ParentId.Equals(config.Id)).ToList();

            var configDTO = this.AutoMapper.Map<ImportConfigDTO>(config);

            configDTO.Children = this.AutoMapper.Map<List<ImportConfigDetailDTO>>(configDetails);

            return configDTO;
        }

        #endregion
    }
}
