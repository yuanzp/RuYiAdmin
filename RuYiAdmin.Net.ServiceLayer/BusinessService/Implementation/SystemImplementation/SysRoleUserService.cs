﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.FrameworkInterface;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.SystemInterface;
using RuYiAdmin.Net.ServiceLayer.BaseService.Implementation;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;
using System.Linq;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Implementation.SystemImplementation
{
    /// <summary>
    /// 角色用户业务层实现
    /// </summary>
    class SysRoleUserService : RuYiAdminBaseService<SysRoleUser>, ISysRoleUserService
    {
        #region 属性及构造函数

        /// <summary>
        /// 角色与用户仓储实例
        /// </summary>
        private readonly ISysRoleUserRepository RoleUserRepository;

        /// <summary>
        /// Redis仓储实例
        /// </summary>
        private readonly IRedisRepository RedisRepository;

        /// <summary>
        /// 业务日志仓储接口实例
        /// </summary>
        private readonly ILoggerRepository LoggerRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="RoleUserRepository"></param>
        /// <param name="RedisRepository"></param>
        /// <param name="LoggerRepository"></param>
        public SysRoleUserService(ISysRoleUserRepository RoleUserRepository,
                                  IRedisRepository RedisRepository,
                                  ILoggerRepository LoggerRepository) : base(RoleUserRepository)
        {
            this.RoleUserRepository = RoleUserRepository;
            this.RedisRepository = RedisRepository;
            this.LoggerRepository = LoggerRepository;
        }

        #endregion

        #region 服务层公有方法

        #region 加载角色与用户缓存

        /// <summary>
        /// 加载角色与用户缓存
        /// </summary>
        public async Task LoadSystemRoleUserCache()
        {
            int totalCount = 0;
            var roleUsers = (await this.RoleUserRepository.DefaultSqlQueryAsync(new SearchCondition(), totalCount)).ToList();
            await this.RedisRepository.SetAsync(RuYiGlobalConfig.SystemCacheConfig.RoleAndUserCacheName, roleUsers, -1);

            this.LoggerRepository.Info("系统角色与用户缓存加载完成");
        }

        #endregion

        #region 清理角色与用户缓存

        /// <summary>
        /// 清理角色与用户缓存
        /// </summary>
        public async Task ClearSystemRoleUserCache()
        {
            await this.RedisRepository.DeleteAsync(new string[] { RuYiGlobalConfig.SystemCacheConfig.RoleAndUserCacheName });

            this.LoggerRepository.Info("系统角色与用户缓存清理完成");
        }

        #endregion

        #endregion
    }
}
