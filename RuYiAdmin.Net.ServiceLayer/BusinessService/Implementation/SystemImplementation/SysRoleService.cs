﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.FrameworkInterface;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.SystemInterface;
using RuYiAdmin.Net.ServiceLayer.BaseService.Implementation;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Implementation.SystemImplementation
{
    /// <summary>
    /// 角色业务层实现
    /// </summary>
    class SysRoleService : RuYiAdminBaseService<SysRole>, ISysRoleService
    {
        #region 属性及构造函数

        /// <summary>
        /// 角色仓储实例
        /// </summary>
        private readonly ISysRoleRepository RoleRepository;

        /// <summary>
        /// Redis仓储实例
        /// </summary>
        private readonly IRedisRepository RedisRepository;

        /// <summary>
        /// 业务日志仓储接口实例
        /// </summary>
        private readonly ILoggerRepository LoggerRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="RoleRepository"></param>
        /// <param name="RedisRepository"></param>
        /// <param name="LoggerRepository"></param>
        public SysRoleService(ISysRoleRepository RoleRepository,
                              IRedisRepository RedisRepository,
                              ILoggerRepository LoggerRepository) : base(RoleRepository)
        {
            this.RoleRepository = RoleRepository;
            this.RedisRepository = RedisRepository;
            this.LoggerRepository = LoggerRepository;
        }

        #endregion

        #region 服务层公有方法

        #region 加载系统角色缓存

        /// <summary>
        /// 加载系统角色缓存
        /// </summary>
        public async Task LoadSystemRoleCache()
        {
            var strSQL = this.GetSqlByKey("sqls:sql:query_role_info");

            int totalCount = 0;
            var roles = await this.RoleRepository.SqlQueryAsync<SysRoleDTO>(new SearchCondition(), totalCount, strSQL);
            await this.RedisRepository.SetAsync(RuYiGlobalConfig.SystemCacheConfig.RoleCacheName, roles, -1);

            this.LoggerRepository.Info("系统角色缓存加载完成");
        }

        #endregion

        #region 清理系统角色缓存

        /// <summary>
        /// 清理系统角色缓存
        /// </summary>
        public async Task ClearSystemRoleCache()
        {
            await this.RedisRepository.DeleteAsync(new string[] { RuYiGlobalConfig.SystemCacheConfig.RoleCacheName });

            this.LoggerRepository.Info("系统菜单缓存清理完成");
        }

        #endregion

        #endregion
    }
}
