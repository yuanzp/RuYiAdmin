﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.AspNetCore.Http;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.EntityDataModel.SessionScope;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.SystemInterface;
using RuYiAdmin.Net.ServiceLayer.BaseService.Implementation;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;
using System;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Implementation.SystemImplementation
{
    /// <summary>
    /// 收件人服务层实现
    /// </summary>
    public class SysAddresseeService : RuYiAdminBaseService<SysAddressee>, ISysAddresseeService
    {
        #region 属性及构造函数

        /// <summary>
        /// 仓储实例
        /// </summary>
        private readonly ISysAddresseeRepository AddresseeRepository;

        /// <summary>
        /// HttpContext
        /// </summary>
        private readonly IHttpContextAccessor ContextAccessor;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="AddresseeRepository"></param>
        /// <param name="ContextAccessor"></param>
        public SysAddresseeService(ISysAddresseeRepository AddresseeRepository,
                                   IHttpContextAccessor ContextAccessor) : base(AddresseeRepository)
        {
            this.AddresseeRepository = AddresseeRepository;
            this.ContextAccessor = ContextAccessor;
        }

        #endregion

        #region 更改通知收件人阅读状态

        /// <summary>
        /// 更改通知收件人阅读状态
        /// </summary>
        /// <param name="notificationId">通知编号</param>
        /// <returns>ActionResponseResult</returns>
        public async Task<ActionResponseResult> UpdateNotificationStatus(Guid notificationId)
        {
            //获取用户信息
            var user = RuYiSessionContext.GetCurrentUserInfo(this.ContextAccessor);

            //获取通知信息
            var addressee = (await this.AddresseeRepository.GetListAsync())
                .Find(t => t.IsDel == (int)DeletionType.Undeleted && t.BusinessId == notificationId && t.UserId == user.Id);
            if (addressee != null && addressee.Status == (int)ReadingStatus.Unread)
            {
                addressee.Status = (int)ReadingStatus.Read;
                await this.AddresseeRepository.UpdateEntityAsync(addressee);
            }

            return ActionResponseResult.Success("OK");
        }

        #endregion
    }
}
