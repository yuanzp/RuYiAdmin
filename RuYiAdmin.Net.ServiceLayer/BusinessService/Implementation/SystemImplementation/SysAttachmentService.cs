﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Contexts;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.SystemInterface;
using RuYiAdmin.Net.ServiceLayer.BaseService.Implementation;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;
using System;
using System.IO;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Implementation.SystemImplementation
{
    /// <summary>
    /// 业务附件服务层实现
    /// </summary>
    public class SysAttachmentService : RuYiAdminBaseService<SysAttachment>, ISysAttachmentService
    {
        #region 属性及构造函数

        /// <summary>
        /// 业务附件仓储实例
        /// </summary>
        private readonly ISysAttachmentRepository AttachmentRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="AttachmentRepository"></param>
        public SysAttachmentService(ISysAttachmentRepository AttachmentRepository) : base(AttachmentRepository)
        {
            this.AttachmentRepository = AttachmentRepository;
        }

        #endregion

        #region 系统文件统计

        /// <summary>
        /// 系统文件统计（MB）
        /// </summary>
        /// <returns>ActionResponseResult</returns>
        public Task<ActionResponseResult> QuerySysFileStatisticalInfo()
        {
            return Task.Run(() =>
            {
                //临时文件
                var tempFileLength = RuYiFileContext.GetFileNames(RuYiGlobalConfig.DirectoryConfig.GetTempPath()).Length;
                var tempFileSize = RuYiFileContext.GetDirectoryFileSizeByMB(RuYiGlobalConfig.DirectoryConfig.GetTempPath());

                //审计日志
                var monitoringLogLength = RuYiFileContext.GetFileNames(RuYiGlobalConfig.DirectoryConfig.GetMonitoringLogsPath()).Length;
                var monitoringLogSize = RuYiFileContext.GetDirectoryFileSizeByMB(RuYiGlobalConfig.DirectoryConfig.GetMonitoringLogsPath());

                //业务附件
                var businessAttachmentLength = RuYiFileContext.GetFileNames(RuYiGlobalConfig.DirectoryConfig.GetBusinessAttachmentPath()).Length;
                var businessAttachmentSize = RuYiFileContext.GetDirectoryFileSizeByMB(RuYiGlobalConfig.DirectoryConfig.GetBusinessAttachmentPath());

                var logPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "RuYiAdminLogs/");

                //Debug文件
                var debugLogLength = RuYiFileContext.GetFileNames(logPath, "Debug*.log", true).Length;
                var debugLogSize = RuYiFileContext.GetDirectoryFileSizeByMB(logPath, "Debug*.log", true);

                //Error文件
                var errorLogLength = RuYiFileContext.GetFileNames(logPath, "Error*.log", true).Length;
                var errorLogSize = RuYiFileContext.GetDirectoryFileSizeByMB(logPath, "Error*.log", true);

                //Info文件
                var infoLogLength = RuYiFileContext.GetFileNames(logPath, "Info*.log", true).Length;
                var infoLogSize = RuYiFileContext.GetDirectoryFileSizeByMB(logPath, "Info*.log", true);

                //Warn文件
                var warnLogLength = RuYiFileContext.GetFileNames(logPath, "Warn*.log", true).Length;
                var warnLogSize = RuYiFileContext.GetDirectoryFileSizeByMB(logPath, "Warn*.log", true);

                var obj = new
                {
                    TempFilesLength = tempFileLength,
                    TempFileSize = tempFileSize,
                    MonitoringLogLength = monitoringLogLength,
                    MonitoringLogSize = monitoringLogSize,
                    BusinessAttachmentLength = businessAttachmentLength,
                    BusinessAttachmentSize = businessAttachmentSize,
                    DebugLogLength = debugLogLength,
                    DebugLogSize = debugLogSize,
                    ErrorLogLength = errorLogLength,
                    ErrorLogSize = errorLogSize,
                    InfoLogLength = infoLogLength,
                    InfoLogSize = infoLogSize,
                    WarnLogLength = warnLogLength,
                    WarnLogSize = warnLogSize
                };

                return ActionResponseResult.Success(obj);
            });
        }

        #endregion
    }
}
