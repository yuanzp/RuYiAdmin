﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Quartz;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Contexts;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessQuartzJobs
{
    /// <summary>
    /// 定时邮件作业
    /// </summary>
    public class EmailJob : IJob
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public EmailJob()
        {
        }

        /// <summary>
        /// 定时邮件作业执行
        /// </summary>
        /// <param name="context">作业执行对象</param>
        /// <returns></returns>
        public Task Execute(IJobExecutionContext context)
        {
            //执行定时发送邮件作业
            RuYiLoggerContext.Info("Email Job Executed!");
            return Task.CompletedTask;
        }
    }
}