using System;

namespace RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminClass
{
    public class WeatherForecast
    {
        /// <summary>
        /// ����
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// TemperatureC
        /// </summary>
        public int TemperatureC { get; set; }

        /// <summary>
        /// TemperatureF
        /// </summary>
        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        /// <summary>
        /// �ܽ�
        /// </summary>
        public string Summary { get; set; }
    }
}
