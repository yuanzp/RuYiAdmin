﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Constants.Business;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.FrameworkInterface;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminExtension
{
    /// <summary>
    /// 分布式消息订阅器
    /// </summary>
    public class DistributedMessageSubscriber : IHostedService
    {
        #region 属性及构造函数

        /// <summary>
        /// ServiceProvider实例
        /// </summary>
        private readonly IServiceProvider serviceProvider;

        /// <summary>
        /// 计划任务接口实例
        /// </summary>
        private readonly ISysScheduleJobService scheduleJobService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="scheduleJobService"></param>
        public DistributedMessageSubscriber(IServiceProvider serviceProvider, ISysScheduleJobService scheduleJobService)
        {
            this.serviceProvider = serviceProvider;
            this.scheduleJobService = scheduleJobService;
        }

        #endregion

        #region 开始事件

        public Task StartAsync(CancellationToken cancellationToken)
        {
            if (RuYiGlobalConfig.QuartzConfig.SupportGroup)
            {
                Task.Run(() =>
                {
                    using (var scope = serviceProvider.CreateScope())
                    {
                        var redisService = scope.ServiceProvider.GetService<IRedisService>();
                        redisService.SubscribeMessage(RuYiGlobalConfig.QuartzConfig.ChanelName, new Action<string>(async message =>
                        {
                            var msg = JsonConvert.DeserializeObject<QuartzJobDTO>(message.ToString());
                            if (msg.GroupId != null && msg.GroupId == RuYiGlobalConfig.QuartzConfig.GroupId)
                            {
                                switch (msg.Action)
                                {
                                    case QuartzJobAction.DELETE:
                                        await scheduleJobService.DeleteScheduleJobAsync(msg.JobId, msg.UserId);
                                        break;
                                    case QuartzJobAction.START:
                                        await scheduleJobService.StartScheduleJobAsync(msg.JobId, msg.UserId);
                                        break;
                                    case QuartzJobAction.PAUSE:
                                        await scheduleJobService.PauseScheduleJobAsync(msg.JobId, msg.UserId);
                                        break;
                                    case QuartzJobAction.RESUME:
                                        await scheduleJobService.ResumeScheduleJobAsync(msg.JobId, msg.UserId);
                                        break;
                                    default: break;
                                }
                            }
                        }));
                    }
                });
            }
            return Task.CompletedTask;
        }

        #endregion

        #region 停止事件

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        #endregion
    }
}
