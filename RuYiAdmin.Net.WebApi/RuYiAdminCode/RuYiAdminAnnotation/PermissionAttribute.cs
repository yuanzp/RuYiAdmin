﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Constants.System;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Contexts;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Utils;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminAnnotation
{
    /// <summary>
    /// 动作权限过滤器
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class PermissionAttribute : ActionFilterAttribute
    {
        private string permission { get; set; }

        public PermissionAttribute(string permission)
        {
            this.permission = permission;
        }

        /// <summary>
        /// 动作鉴权
        /// </summary>
        /// <param name="context"></param>
        /// <exception cref="Exception"></exception>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);

            if (string.IsNullOrEmpty(permission))
            {
                context.Result = new BadRequestObjectResult(ExceptionMessage.NotNullPermissionExceptionMessage);
                return;
            }

            var token = context.HttpContext.GetToken();
            //获取用户
            var user = RuYiRedisContext.Get<SysUserDTO>(token);
            if (user == null)
            {
                context.Result = new UnauthorizedObjectResult(ExceptionMessage.InvalidTokenExceptionMessage);
                return;
            }

            //放行超级用户
            if (user.IsSupperAdmin.Equals((int)YesNo.YES) && user.OrgId.Equals(Guid.Empty))
            {
                return;
            }

            var permissionArray = permission.Split(',');

            #region 获取用户角色

            var listRole = RuYiRedisContext.Get<List<SysRoleUser>>(RuYiGlobalConfig.SystemCacheConfig.RoleAndUserCacheName);

            var roleIds = listRole.Where(t => t.IsDel == (int)DeletionType.Undeleted && t.UserId.Equals(user.Id)).Select(t => t.RoleId).ToArray();

            #endregion

            #region  获取角色菜单

            var roleMenuList = RuYiRedisContext.Get<List<SysRoleMenu>>(RuYiGlobalConfig.SystemCacheConfig.RoleAndMenuCacheName);

            var listRoleMenu = new List<SysRoleMenu>();

            foreach (var item in roleIds)
            {
                var roleMenus = roleMenuList.Where(t => t.IsDel == (int)DeletionType.Undeleted && t.RoleId.Equals(item)).ToList();
                listRoleMenu.AddRange(roleMenus);
            }

            var menuIds = listRoleMenu.Select(t => t.MenuId).Distinct().ToArray();

            #endregion

            #region 获取系统菜单

            var menus = RuYiRedisContext.Get<List<SysMenuDTO>>(RuYiGlobalConfig.SystemCacheConfig.MenuCacheName);

            var listMenus = menus.Where(t => t.IsDel == (int)DeletionType.Undeleted
                                          && t.MenuType.Equals(MenuType.Button)
                                          || t.MenuType.Equals(MenuType.View)).ToList();

            #endregion

            #region 操作权限判断

            var result = false;

            foreach (var item in menuIds)
            {
                var menu = listMenus.Where(t => t.Id.Equals(item)).FirstOrDefault();

                if (menu != null && !string.IsNullOrEmpty(menu.Code))
                {
                    //是否包含权限组
                    if (permissionArray.Contains(menu.Code))
                    {
                        result = true;
                        break;
                    }
                }
            }

            #endregion

            if (!result)
            {
                context.Result = new ForbidResult(WarnningMessage.AccessDeniedMessage);
                return;
            }
        }
    }
}
