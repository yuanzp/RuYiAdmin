﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Utils;
using RuYiAdmin.Net.EntityDataModel.BaseEntityModel;
using RuYiAdmin.Net.ServiceLayer.BaseService.Interface;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminAnnotation;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminFilter;
using System;
using System.IO;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminBase
{
    /// <summary>
    /// 控制器基类
    /// </summary>
    [Authorize]
    [ActionAuthorization]
    [ApiController]
    [Route(RuYiGlobalConfig.RouteTemplate)]
    public class RuYiAdminBaseController<T> : ControllerBase where T : RuYiAdminBaseEntity
    {
        /*声明：

        1.该基类方法可用于构建独立的WebApi。

        2.方法不够用时可调用基类服务、自行添加。

        3.按钮与视图的权限鉴别，请在具体的业务控制器中实现。

        */

        #region 属性与构造函数

        /// <summary>
        /// 服务层基类实例
        /// </summary>
        private readonly IRuYiAdminBaseService<T> RuYiAdminBaseService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="RuYiAdminBaseService"></param>
        public RuYiAdminBaseController(IRuYiAdminBaseService<T> RuYiAdminBaseService)
        {
            this.RuYiAdminBaseService = RuYiAdminBaseService;
        }

        #endregion        

        #region 通用方法

        /// <summary>
        /// 分页查询列表
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.QueryList)]
        public async Task<IActionResult> GetPage(SearchCondition searchCondition)
        {
            var actionResponseResult = await RuYiAdminBaseService.GetPageAsync(searchCondition);
            return Ok(actionResponseResult);
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.QueryList)]
        public async Task<IActionResult> GetList(SearchCondition searchCondition)
        {
            var actionResponseResult = await RuYiAdminBaseService.GetListAsync(searchCondition);
            return Ok(actionResponseResult);
        }

        /// <summary>
        /// 按编号查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Log(OperationType.QueryEntity)]
        public async Task<IActionResult> GetEntityById(Guid id)
        {
            var actionResponseResult = await RuYiAdminBaseService.GetByIdAsync(id);
            return Ok(actionResponseResult);
        }

        /// <summary>
        /// 新增对象
        /// </summary>
        /// <param name="t">对象</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.AddEntity)]
        public async Task<IActionResult> AddEntity([FromBody] T t)
        {
            var actionResponseResult = await RuYiAdminBaseService.AddAsync(t);
            return Ok(actionResponseResult);
        }

        /// <summary>
        /// 编辑对象
        /// </summary>
        /// <param name="t">对象</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPut]
        [Log(OperationType.EditEntity)]
        public async Task<IActionResult> EditEntity([FromBody] T t)
        {
            var actionResponseResult = await RuYiAdminBaseService.UpdateAsync(t);
            return Ok(actionResponseResult);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpDelete("{id}")]
        [Log(OperationType.DeleteEntity)]
        public async Task<IActionResult> DeleteEntityById(Guid id)
        {
            var actionResponseResult = await RuYiAdminBaseService.DeleteAsync(id);
            return Ok(actionResponseResult);
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="ids">编号数组</param>
        /// <returns>ActionResponseResult</returns>
        [HttpDelete("{ids}")]
        [Log(OperationType.DeleteEntity)]
        public async Task<IActionResult> DeleteEntitiesByIds(string ids)
        {
            var array = RuYiStringUtil.GetGuids(ids);
            var actionResponseResult = await RuYiAdminBaseService.DeleteRangeAsync(array);
            return Ok(actionResponseResult);
        }

        /// <summary>
        /// 下载Excel
        /// </summary>
        /// <param name="excelId">文件编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpGet("{excelId}")]
        [Log(OperationType.DownloadFile)]
        public async Task<IActionResult> DownloadExcel(string excelId)
        {
            return await Task.Run(() =>
            {
                //存储路径
                var path = Path.Join(RuYiGlobalConfig.DirectoryConfig.GetTempPath(), "/");
                //文件路径
                var filePath = Path.Join(path, excelId + ".xls");
                //文件读写流
                var stream = new FileStream(filePath, FileMode.Open);
                //设置流的起始位置
                stream.Position = 0;

                return File(stream, "application/octet-stream", "错误信息.xls");
            });
        }

        /// <summary>
        /// 下载Excel模板
        /// </summary>
        /// <param name="templateId">文件编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpGet("{templateId}")]
        [Log(OperationType.DownloadFile)]
        public async Task<IActionResult> DownloadTemplate(string templateId)
        {
            return await Task.Run(() =>
            {
                //存储路径
                var path = Path.Join(RuYiGlobalConfig.DirectoryConfig.GetTemplateDirectory(), "/");
                //文件路径
                var filePath = Path.Join(path, templateId + ".xls");
                //文件读写流
                var stream = new FileStream(filePath, FileMode.Open);
                //设置流的起始位置
                stream.Position = 0;

                return File(stream, "application/octet-stream", "导入模板.xls");
            });
        }

        /// <summary>
        /// 下载Zip
        /// </summary>
        /// <param name="zipId">文件编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpGet("{zipId}")]
        [Log(OperationType.DownloadFile)]
        public async Task<IActionResult> DownloadZip(string zipId)
        {
            return await Task.Run(() =>
            {
                //存储路径
                var path = Path.Join(RuYiGlobalConfig.DirectoryConfig.GetTempPath(), "/");
                //文件路径
                var filePath = Path.Join(path, zipId + ".zip");
                //文件读写流
                var stream = new FileStream(filePath, FileMode.Open);
                //设置流的起始位置
                stream.Position = 0;

                return File(stream, "application/octet-stream", "RuYiAdmin.zip");
            });
        }

        #endregion
    }
}
