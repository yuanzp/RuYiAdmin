﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RuYiAdmin.Net.WebApi.RuYiAdminController.FrameworkController
{
    [Route(RuYiGlobalConfig.RouteTemplate)]
    [ApiController]
    public class RuYiAdminSystemController : ControllerBase
    {
        #region 获取消息中间件类型

        /// <summary>
        /// 获取消息中间件类型
        /// </summary>
        /// <returns>ActionResponseResult</returns>
        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetMomType()
        {
            var actionResponseResult = ActionResponseResult.Success((int)RuYiGlobalConfig.MomConfig.MomType);
            return Ok(actionResponseResult);
        }

        #endregion
    }
}
