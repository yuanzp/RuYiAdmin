﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Exceptions.Framework;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminFilter;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RuYiAdmin.Net.WebApi.RuYiAdminController.FrameworkController
{
    [Authorize]
    [ActionAuthorization]
    [ApiController]
    [Route(RuYiGlobalConfig.RouteTemplate)]
    public class RuYiAdminApolloController : ControllerBase
    {
        /// <summary>
        /// 输出实例
        /// </summary>
        private readonly ILogger<RuYiAdminApolloController> logger;

        /// <summary>
        /// 全局配置
        /// </summary>
        private readonly IConfiguration configuration;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="logger">输出实例</param>
        /// <param name="configuration">全局配置</param>
        public RuYiAdminApolloController(ILogger<RuYiAdminApolloController> logger, IConfiguration configuration)
        {
            this.logger = logger;
            this.configuration = configuration;
        }

        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new RuYiAdminCustomException("key can not be empty");
            }

            var value = configuration[key];
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new RuYiAdminCustomException("value is null");
            }

            logger.LogInformation($"Apollo:{key},{value}");

            return Ok(value);
        }
    }
}
