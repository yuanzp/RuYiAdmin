﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Constants.Framework;
using RuYiAdmin.Net.CommonInfrastructure.Constants.System;
using RuYiAdmin.Net.CommonInfrastructure.Exceptions.Framework;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Utils;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.FrameworkInterface;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminClass;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RuYiAdmin.Net.WebApi.RuYiAdminController.FrameworkController
{
    /// <summary>
    /// Jwt接口认证控制器
    /// </summary>
    [ApiController]
    [Route(RuYiGlobalConfig.RouteTemplate)]
    public class JwtSecurityAuthenticationController : ControllerBase
    {
        #region 属性及构造函数

        /// <summary>
        /// Redis接口实例
        /// </summary>
        private readonly IRedisService RedisService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="RedisService"></param>
        public JwtSecurityAuthenticationController(IRedisService RedisService)
        {
            this.RedisService = RedisService;
        }

        #endregion

        #region 获取盐份

        /// <summary>
        /// 获取盐份
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns>盐份</returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Get(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                throw new RuYiAdminCustomException(ExceptionMessage.NotNullUserNameExceptionMessage);
            }

            if (userName.Equals(RuYiGlobalConfig.JwtSettings.DefaultUser))
            {
                var salt = Guid.NewGuid();
                var encrytStr = RuYiHashUtil.HashToHexString512(RuYiGlobalConfig.JwtSettings.DefaultPassword + salt);
                await this.RedisService.SetAsync(salt.ToString(), encrytStr, RuYiGlobalConfig.JwtSettings.SaltExpiration);

                var actionResponseResult = ActionResponseResult.Success(salt);
                return Ok(actionResponseResult);
            }

            throw new RuYiAdminCustomException(WarnningMessage.NoContentMessage);
        }

        #endregion

        #region 获取Jwt口令

        /// <summary>
        /// Jwt身份认证接口
        /// </summary>
        /// <param name="jwtAuthentication">身份信息</param>
        /// <returns>token信息</returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Post([FromBody] JwtAuthentication jwtAuthentication)
        {
            if (!ModelState.IsValid)
            {
                throw new RuYiAdminCustomException(ExceptionMessage.InvalidModelStateExceptionMessage);
            }

            var encrytStr = string.Empty;

            if (await this.RedisService.ExistsAsync(new string[] { jwtAuthentication.Salt }) > 0)
            {
                encrytStr = await this.RedisService.GetAsync(jwtAuthentication.Salt);
            }

            if (string.IsNullOrEmpty(encrytStr))
            {
                throw new RuYiAdminCustomException(ExceptionMessage.InvalidSaltExceptionMessage);
            }

            if (jwtAuthentication.UserName.Equals(RuYiGlobalConfig.JwtSettings.DefaultUser)
                && jwtAuthentication.Password.Equals(encrytStr, StringComparison.OrdinalIgnoreCase))
            {
                await this.RedisService.DeleteAsync(new string[] { jwtAuthentication.Salt });

                var token = JwtAuthentication.GetJwtSecurityToken(jwtAuthentication.UserName);

                //设置RefreshToken有效期
                await this.RedisService.SetAsync(token.Id, token.Id, 12 * 3 * RuYiGlobalConfig.JwtSettings.TokenExpiration * 60);

                var actionResponseResult = new ActionResponseResult()
                {
                    HttpStatusCode = HttpStatusCode.OK,
                    Object = new { AccessToken = new JwtSecurityTokenHandler().WriteToken(token), RefreshToken = token.Id },
                    Message = "OK,expiration:" + token.ValidTo.ToLocalTime()
                };

                return Ok(actionResponseResult);
            }

            throw new RuYiAdminCustomException(WarnningMessage.NotFindMessage);
        }

        #endregion

        #region 刷新Jwt口令

        /// <summary>
        /// 刷新口令
        /// </summary>
        /// <returns>ActionResponseResult</returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> RefreshToken()
        {
            string accessToken = HttpContext.Request.Headers[Keywords.AUTHORIZATION];
            string refreshToken = HttpContext.Request.Headers[Keywords.REFRESHTOKEN];

            var jwtToken = new JwtSecurityTokenHandler().ReadJwtToken(accessToken.Replace(Keywords.BEARER, ""));
            if (jwtToken.Id.Equals(refreshToken))
            {
                var rtValue = await this.RedisService.GetAsync(refreshToken);
                if (!string.IsNullOrEmpty(rtValue) && rtValue.Equals(refreshToken))
                {
                    //删除旧RefreshToken
                    await this.RedisService.DeleteAsync(new string[] { refreshToken });

                    var token = JwtAuthentication.GetJwtSecurityToken(RuYiGlobalConfig.JwtSettings.DefaultUser);

                    //设置RefreshToken有效期
                    await this.RedisService.SetAsync(token.Id, token.Id, 12 * 3 * RuYiGlobalConfig.JwtSettings.TokenExpiration * 60);

                    var actionResponseResult = new ActionResponseResult()
                    {
                        HttpStatusCode = HttpStatusCode.OK,
                        Object = new { AccessToken = new JwtSecurityTokenHandler().WriteToken(token), RefreshToken = token.Id },
                        Message = "OK,expiration:" + token.ValidTo.ToLocalTime()
                    };

                    return Ok(actionResponseResult);
                }
            }

            throw new RuYiAdminCustomException(WarnningMessage.ForbiddenMessage);
        }

        #endregion
    }
}
