﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.WebApi.RuYiAdminController.BusinessController
{
    /// <summary>
    /// 天气信息
    /// </summary>
    [ApiController]
    [AllowAnonymous]
    [Route(RuYiGlobalConfig.RouteTemplate)]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> logger;
        private readonly IMemoryCache memoryCache;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IMemoryCache memoryCache)
        {
            this.logger = logger;
            this.memoryCache = memoryCache;
        }

        /// <summary>
        /// 获取天气信息
        /// </summary>
        /// <returns>天气信息</returns>
        [HttpGet]
        public async Task<IEnumerable<WeatherForecast>> Get()
        {
            return await Task.Run(() =>
            {
                var rng = new Random();

                memoryCache.Set("Random", rng.Next(-20, 55), TimeSpan.FromSeconds(120));
                logger.LogInformation(memoryCache.Get("Random").ToString());

                return Enumerable.Range(1, 5).Select(index => new WeatherForecast
                {
                    Date = DateTime.Now.AddDays(index),
                    TemperatureC = rng.Next(-20, 55),
                    Summary = Summaries[rng.Next(Summaries.Length)]
                })
                .ToArray();
            });
        }

    }
}
