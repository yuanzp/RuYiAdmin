//-----------------------------------------------------------------------
// <Copyright file="BizUserManagementController.cs" company="RuYiAdmin">
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// * Version : 4.0.30319.42000
// * Author  : auto generated by RuYiAdmin T4 Template
// * FileName: BizUserManagementController.cs
// * History : Created by RuYiAdmin 01/21/2022 13:22:06
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RuYiAdmin.Net.CommonInfrastructure.Classes;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Constants.System;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.CommonInfrastructure.Exceptions.Framework;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Utils;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.BusinessModel.ModuleManagement;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel;
using RuYiAdmin.Net.EntityDataModel.EntityModel.BusinessModel.ModuleManagement;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.BusinessInterface.ModuleManagement;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.FrameworkInterface;
using RuYiAdmin.Net.ServiceLayer.BusinessServiceExtension;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminAnnotation;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminBase;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.WebApi.RuYiAdminController.BusinessController.ModuleManagement
{
    /// <summary>
    /// BizUser控制器
    /// </summary>
    public class BizUserManagementController : RuYiAdminBaseController<BizUser>
    {
        #region 属性及构造函数

        /// <summary>
        /// 业务模块接口实例
        /// </summary>
        private readonly IBizModuleService BizModuleService;

        /// <summary>
        /// 模块用户接口实例
        /// </summary>
        private readonly IBizUserModuleService BizUserModuleService;

        /// <summary>
        /// 业务接口实例
        /// </summary>
        private readonly IBizUserService BizUserService;

        /// <summary>
        /// Redis接口实例
        /// </summary>
        private readonly IRedisService RedisService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="BizUserService"></param>
        /// <param name="BizUserModuleService"></param>
        /// <param name="BizModuleService"></param>
        /// <param name="RedisService"></param>
        public BizUserManagementController(IBizUserService BizUserService,
                                           IBizUserModuleService BizUserModuleService,
                                           IBizModuleService BizModuleService,
                                           IRedisService RedisService) : base(BizUserService)
        {
            this.BizUserService = BizUserService;
            this.BizUserModuleService = BizUserModuleService;
            this.BizModuleService = BizModuleService;
            this.RedisService = RedisService;
        }

        #endregion

        #region 同步新增模块用户

        /// <summary>
        /// 同步新增模块用户
        /// </summary>
        /// <param name="bizUser">模块用户对象</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Add([FromBody] BizUserDTO bizUser)
        {
            var arrary = RuYiRsaUtil.PemDecrypt(bizUser.UserPassword, RuYiGlobalConfig.SystemConfig.RsaPrivateKey);
            var password = arrary.Split('_')[0];
            var token = arrary.Split('_')[1];

            var accountDTO = await this.RedisService.GetAsync<BizAccountDTO>(RuYiGlobalConfig.RedisConfig.SynchronizationPattern + token);
            if (accountDTO != null && accountDTO.Token.Equals(token))
            {
                var task = await this.BizModuleService.GetByIdAsync(bizUser.ModuleId);
                var module = (BizModule)task.Object;
                var users = (await this.BizUserService.GetListAsync()).Object as List<BizUser>;

                bizUser.UserLogonName = RuYiRsaUtil.PemDecrypt(bizUser.UserLogonName, RuYiGlobalConfig.SystemConfig.RsaPrivateKey);
                bizUser.UserLogonName = bizUser.UserLogonName.Split('_')[0];

                string logonName = string.Join('_', module.ModuleShortNameEN, bizUser.UserLogonName);
                var user = users.Where(t => t.UserLogonName.Equals(logonName)).FirstOrDefault();
                if (user != null)
                {
                    throw new RuYiAdminCustomException(ExceptionMessage.DuplicatedLogonNameExceptionMessage);
                }

                #region 新增业务模块用户

                var defaultPassword = RuYiGlobalConfig.SystemConfig.DefaultPassword;
                var aesKey = RuYiGlobalConfig.SystemConfig.AesKey;

                user = new BizUser();
                user.UserLogonName = string.Join('_', module.ModuleShortNameEN, bizUser.UserLogonName);
                user.UserDisplayName = bizUser.UserDisplayName;
                user.Salt = Guid.NewGuid();
                user.UserPassword = RuYiAesUtil.Encrypt(defaultPassword + user.Salt, aesKey);
                user.Telephone = bizUser.Telephone;
                user.MobilePhone = bizUser.MobilePhone;
                user.Email = bizUser.Email;
                user.Sex = bizUser.Sex;
                user.IsEnabled = (int)YesNo.YES;
                user.IsDel = (int)DeletionType.Undeleted;
                user.Creator = accountDTO.Id;
                user.CreateTime = DateTime.Now;
                user.Modifier = accountDTO.Id;
                user.ModifyTime = DateTime.Now;
                user.VersionId = Guid.NewGuid();
                await RuYiAdminDbScope.RuYiDbContext.Insertable(user).ExecuteCommandAsync();

                #endregion

                #region 新增模块与用户关系

                var userModule = new BizUserModule();
                userModule.UserId = user.Id;
                userModule.ModuleId = bizUser.ModuleId;
                userModule.UserModuleLogonName = bizUser.UserLogonName;
                userModule.UserModulePassword = password;
                userModule.IsDel = (int)DeletionType.Undeleted;
                userModule.Creator = accountDTO.Id;
                userModule.CreateTime = DateTime.Now;
                userModule.Modifier = accountDTO.Id;
                userModule.ModifyTime = DateTime.Now;
                userModule.VersionId = Guid.NewGuid();
                await RuYiAdminDbScope.RuYiDbContext.Insertable(userModule).ExecuteCommandAsync();

                #endregion

                #region 记录同步新增用户日志

                var log = new SysLog();

                log.Id = Guid.NewGuid();

                log.UserId = accountDTO.Id;
                log.UserName = accountDTO.UserDisplayName + "/" + accountDTO.UserLogonName;
                log.OrgId = module.Id;
                log.OrgName = module.ModuleShortName;

                log.System = this.HttpContext.Request.Headers["User-Agent"].FirstOrDefault().ToString().Split('(')[1].Split(')')[0];
                log.Browser = this.HttpContext.Request.Headers["sec-ch-ua"];

                var ip = this.HttpContext.Request.Headers["X-Forwarded-For"].FirstOrDefault();
                if (string.IsNullOrEmpty(ip))
                {
                    ip = this.HttpContext.Connection.RemoteIpAddress.ToString();
                }
                log.IP = ip;

                log.OperationType = OperationType.SyncAddUser;
                log.RequestMethod = this.HttpContext.Request.Method;
                log.RequestUrl = "/API/BizUserManagement/Add";
                log.Params = Newtonsoft.Json.JsonConvert.SerializeObject(bizUser);
                log.Result = string.Empty;

                log.OldValue = string.Empty;
                log.NewValue = string.Empty;

                log.Remark = $"{module.ModuleShortName}于{DateTime.Now}访问了{log.RequestUrl}接口";
                log.IsDel = (int)DeletionType.Undeleted;
                log.Creator = accountDTO.Id;
                log.CreateTime = DateTime.Now;
                log.Modifier = accountDTO.Id;
                log.ModifyTime = DateTime.Now;
                log.VersionId = Guid.NewGuid();

                //记录审计日志
                await log.WriteAsync();

                #endregion

                return Ok(ActionResponseResult.OK());
            }

            throw new RuYiAdminCustomException(ExceptionMessage.InvalidTokenExceptionMessage);
        }

        #endregion

        #region 同步编辑模块用户

        /// <summary>
        /// 同步编辑模块用户
        /// </summary>
        /// <param name="bizUser">模块用户对象</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPut]
        [AllowAnonymous]
        public async Task<IActionResult> Put([FromBody] BizUserDTO bizUser)
        {
            var arrary = RuYiRsaUtil.PemDecrypt(bizUser.UserPassword, RuYiGlobalConfig.SystemConfig.RsaPrivateKey);
            var password = arrary.Split('_')[0];
            var token = arrary.Split('_')[1];

            var accountDTO = await this.RedisService.GetAsync<BizAccountDTO>(RuYiGlobalConfig.RedisConfig.SynchronizationPattern + token);
            if (accountDTO != null && accountDTO.Token.Equals(token))
            {
                var task = await this.BizModuleService.GetByIdAsync(bizUser.ModuleId);
                var module = (BizModule)task.Object;

                bizUser.UserLogonName = RuYiRsaUtil.PemDecrypt(bizUser.UserLogonName, RuYiGlobalConfig.SystemConfig.RsaPrivateKey);
                bizUser.UserLogonName = bizUser.UserLogonName.Split('_')[0];

                var user = await this.BizUserService.GetBizUser(module.ModuleShortNameEN, bizUser.UserLogonName);
                if (user != null)
                {
                    user.UserDisplayName = bizUser.UserDisplayName;
                    user.Telephone = bizUser.Telephone;
                    user.MobilePhone = bizUser.MobilePhone;
                    user.Email = bizUser.Email;
                    user.Sex = bizUser.Sex;
                    user.Modifier = accountDTO.Id;
                    user.ModifyTime = DateTime.Now;
                    user.VersionId = Guid.NewGuid();
                    await RuYiAdminDbScope.RuYiDbContext.Updateable(user).ExecuteCommandAsync();

                    var userModule = await this.BizUserModuleService.GetBizUserModule(user.Id, module.Id);
                    if (userModule != null)
                    {
                        userModule.UserModulePassword = password;
                        userModule.Modifier = accountDTO.Id;
                        userModule.ModifyTime = DateTime.Now;
                        userModule.VersionId = Guid.NewGuid();
                        await RuYiAdminDbScope.RuYiDbContext.Updateable(userModule).ExecuteCommandAsync();
                    }

                    #region 记录同步编辑用户日志

                    var log = new SysLog();

                    log.Id = Guid.NewGuid();

                    log.UserId = accountDTO.Id;
                    log.UserName = accountDTO.UserDisplayName + "/" + accountDTO.UserLogonName;
                    log.OrgId = module.Id;
                    log.OrgName = module.ModuleShortName;

                    log.System = this.HttpContext.Request.Headers["User-Agent"].FirstOrDefault().ToString().Split('(')[1].Split(')')[0];
                    log.Browser = this.HttpContext.Request.Headers["sec-ch-ua"];

                    var ip = this.HttpContext.Request.Headers["X-Forwarded-For"].FirstOrDefault();
                    if (string.IsNullOrEmpty(ip))
                    {
                        ip = this.HttpContext.Connection.RemoteIpAddress.ToString();
                    }
                    log.IP = ip;

                    log.OperationType = OperationType.SyncEditUser;
                    log.RequestMethod = this.HttpContext.Request.Method;
                    log.RequestUrl = "/API/BizUserManagement/Put";
                    log.Params = Newtonsoft.Json.JsonConvert.SerializeObject(bizUser);
                    log.Result = string.Empty;

                    log.OldValue = string.Empty;
                    log.NewValue = string.Empty;

                    log.Remark = $"{module.ModuleShortName}于{DateTime.Now}访问了{log.RequestUrl}接口";
                    log.IsDel = (int)DeletionType.Undeleted;
                    log.Creator = accountDTO.Id;
                    log.CreateTime = DateTime.Now;
                    log.Modifier = accountDTO.Id;
                    log.ModifyTime = DateTime.Now;
                    log.VersionId = Guid.NewGuid();

                    //记录审计日志
                    await log.WriteAsync();

                    #endregion

                    return Ok(ActionResponseResult.OK());
                }
                else
                {
                    throw new RuYiAdminCustomException(WarnningMessage.NotFindMessage);
                }
            }

            throw new RuYiAdminCustomException(ExceptionMessage.InvalidTokenExceptionMessage);
        }

        #endregion

        #region 同步删除模块用户

        /// <summary>
        /// 同步删除模块用户
        /// </summary>
        /// <param name="bizUser">模块用户对象</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> DeleteRange(BizUserDTO bizUser)
        {
            var logonNames = RuYiRsaUtil.PemDecrypt(bizUser.UserLogonName, RuYiGlobalConfig.SystemConfig.RsaPrivateKey);
            var token = logonNames.Split('_')[1];

            var accountDTO = await this.RedisService.GetAsync<BizAccountDTO>(RuYiGlobalConfig.RedisConfig.SynchronizationPattern + token);
            if (accountDTO != null && accountDTO.Token.Equals(token))
            {
                var module = (BizModule)(await this.BizModuleService.GetByIdAsync(bizUser.ModuleId)).Object;
                var array = logonNames.Split('_')[0].ToString().Split(',');

                //var range = new List<Guid>();
                var userModuleRange = new List<BizUserModule>();
                foreach (var item in array)
                {
                    var user = await this.BizUserService.GetBizUser(module.ModuleShortNameEN, item);
                    if (user != null)
                    {
                        //range.Add(user.Id);
                        var userModule = await this.BizUserModuleService.GetBizUserModule(user.Id, module.Id);
                        userModuleRange.Add(userModule);
                    }
                    else
                    {
                        throw new RuYiAdminCustomException(bizUser.UserLogonName + " is not existed");
                    }
                }

                //删除业务用户
                //var actionResponseResult = this.BizUserService.DeleteRange(range.ToArray());
                foreach (var item in userModuleRange)
                {
                    item.IsDel = (int)DeletionType.Deleted;
                    item.Modifier = accountDTO.Id;
                    item.ModifyTime = DateTime.Now;
                    item.VersionId = Guid.NewGuid();
                    await RuYiAdminDbScope.RuYiDbContext.Updateable(item).ExecuteCommandAsync();//删除授权关系
                }

                #region 记录同步删除用户日志

                var log = new SysLog();

                log.Id = Guid.NewGuid();

                log.UserId = accountDTO.Id;
                log.UserName = accountDTO.UserDisplayName + "/" + accountDTO.UserLogonName;
                log.OrgId = module.Id;
                log.OrgName = module.ModuleShortName;

                log.System = this.HttpContext.Request.Headers["User-Agent"].FirstOrDefault().ToString().Split('(')[1].Split(')')[0];
                log.Browser = this.HttpContext.Request.Headers["sec-ch-ua"];

                var ip = this.HttpContext.Request.Headers["X-Forwarded-For"].FirstOrDefault();
                if (string.IsNullOrEmpty(ip))
                {
                    ip = this.HttpContext.Connection.RemoteIpAddress.ToString();
                }
                log.IP = ip;

                log.OperationType = OperationType.SyncDeleteUser;
                log.RequestMethod = this.HttpContext.Request.Method;
                log.RequestUrl = "/API/BizUserManagement/DeleteRange";
                log.Params = Newtonsoft.Json.JsonConvert.SerializeObject(bizUser);
                log.Result = string.Empty;

                log.OldValue = string.Empty;
                log.NewValue = string.Empty;

                log.Remark = $"{module.ModuleShortName}于{DateTime.Now}访问了{log.RequestUrl}接口";
                log.IsDel = (int)DeletionType.Undeleted;
                log.Creator = accountDTO.Id;
                log.CreateTime = DateTime.Now;
                log.Modifier = accountDTO.Id;
                log.ModifyTime = DateTime.Now;
                log.VersionId = Guid.NewGuid();

                //记录审计日志
                await log.WriteAsync();

                #endregion

                return Ok(ActionResponseResult.OK());
            }

            throw new RuYiAdminCustomException(ExceptionMessage.InvalidTokenExceptionMessage);
        }

        #endregion

        #region 查询离态用户列表

        /// <summary>
        /// 查询离态用户列表
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.QueryList)]
        [Permission("user:nonmodule:list")]
        public async Task<IActionResult> Post(SearchCondition searchCondition)
        {
            var actionResponseResult = await this.BizUserModuleService.SqlQueryAsync<BizUserModuleDTO>(searchCondition, "sqls:sql:query_biz_user_nonmodule");
            return Ok(actionResponseResult);
        }

        #endregion

        #region 匿名获取盐份服务

        /// <summary>
        /// 匿名获取盐份服务
        /// </summary>
        /// <returns>盐份</returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GetSalt()
        {
            var salt = Guid.NewGuid();
            await this.RedisService.SetAsync(salt.ToString(), salt, RuYiGlobalConfig.JwtSettings.SaltExpiration);
            var actionResponseResult = ActionResponseResult.OK(salt);
            return Ok(actionResponseResult);
        }

        #endregion

        #region 匿名获取同步口令

        /// <summary>
        /// 匿名获取同步口令
        /// </summary>
        /// <param name="loginDTO">登录信息</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> GetToken([FromBody] LoginDTO loginDTO)
        {
            var actionResponseResult = await this.BizUserModuleService.GetToken(loginDTO);
            return Ok(actionResponseResult);
        }

        #endregion
    }
}
