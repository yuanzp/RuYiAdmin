﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Constants.System;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.CommonInfrastructure.Exceptions.Framework;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Utils;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.FrameworkInterface;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminAnnotation;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RuYiAdmin.Net.WebApi.RuYiAdminController.SystemController
{
    /// <summary>
    /// 机构管理控制器
    /// </summary>
    public class SysOrganizationManagementController : RuYiAdminBaseController<SysOrganization>
    {
        #region 属性及构造函数

        /// <summary>
        /// 机构接口实例
        /// </summary>
        private readonly ISysOrganizationService OrganizationService;

        /// <summary>
        /// Redis接口实例
        /// </summary>
        private readonly IRedisService RedisService;

        /// <summary>
        /// AutoMapper实例
        /// </summary>
        private readonly IMapper AutoMapper;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="OrganizationService"></param>
        /// <param name="RedisService"></param>
        /// <param name="AutoMapper"></param>
        public SysOrganizationManagementController(ISysOrganizationService OrganizationService,
                                                   IRedisService RedisService,
                                                   IMapper AutoMapper) : base(OrganizationService)
        {
            this.OrganizationService = OrganizationService;
            this.RedisService = RedisService;
            this.AutoMapper = AutoMapper;
        }

        #endregion

        #region 查询机构列表

        /// <summary>
        /// 查询机构列表
        /// </summary>
        /// <returns>QueryResponseResult</returns>
        [HttpPost]
        [Log(OperationType.QueryList)]
        [Permission("org:query:list")]
        public async Task<IActionResult> Post()
        {
            var actionResponseResult = await this.OrganizationService.GetOrgTreeNodes();
            return Ok(actionResponseResult);
        }

        #endregion

        #region 查询机构信息

        /// <summary>
        /// 查询机构信息
        /// </summary>
        /// <param name="orgId">机构编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpGet("{orgId}")]
        [Log(OperationType.QueryEntity)]
        [Permission("org:query:list")]
        public async Task<IActionResult> GetById(Guid orgId)
        {
            var orgs = await this.RedisService.GetAsync<List<SysOrganizationDTO>>(RuYiGlobalConfig.SystemCacheConfig.OrgCacheName);
            var actionResponseResult = ActionResponseResult.OK(orgs.Where(t => t.Id == orgId).FirstOrDefault());
            return Ok(actionResponseResult);
        }

        #endregion

        #region 新增机构信息

        /// <summary>
        /// 新增机构信息
        /// </summary>
        /// <param name="org">机构对象</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.AddEntity)]
        [Permission("org:add:entity")]
        public async Task<IActionResult> Add([FromBody] SysOrganization org)
        {
            var actionResponseResult = await this.OrganizationService.AddAsync(org);

            //数据一致性维护
            var orgs = await this.RedisService.GetAsync<List<SysOrganizationDTO>>(RuYiGlobalConfig.SystemCacheConfig.OrgCacheName);
            var orgDTO = this.AutoMapper.Map<SysOrganizationDTO>(org);
            orgs.Add(orgDTO);
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.OrgCacheName, orgs, -1);

            return Ok(actionResponseResult);
        }

        #endregion

        #region 编辑机构信息

        /// <summary>
        /// 编辑机构信息
        /// </summary>
        /// <param name="org">机构对象</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPut]
        [Log(OperationType.EditEntity)]
        [Permission("org:edit:entity")]
        public async Task<IActionResult> Put([FromBody] SysOrganization org)
        {
            var actionResponseResult = await this.OrganizationService.UpdateAsync(org);

            #region 数据一致性维护

            //获取机构缓存
            var orgs = await this.RedisService.GetAsync<List<SysOrganizationDTO>>(RuYiGlobalConfig.SystemCacheConfig.OrgCacheName);
            //删除旧数据
            var old = orgs.Where(t => t.Id == org.Id).FirstOrDefault();
            orgs.Remove(old);
            //转化为DTO
            var orgDTO = this.AutoMapper.Map<SysOrganizationDTO>(org);
            if (orgDTO.Leader != null)
            {
                var users = await this.RedisService.GetAsync<List<SysUserDTO>>(RuYiGlobalConfig.SystemCacheConfig.UserCacheName);
                var user = users.Where(t => t.Id == orgDTO.Leader).FirstOrDefault();
                if (user != null)
                {
                    //机构管理人姓名赋值
                    orgDTO.LeaderName = user.DisplayName;
                }
            }
            //添加新数据
            orgs.Add(orgDTO);
            //更新机构缓存
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.OrgCacheName, orgs, -1);

            //机构名称变更时修改用户缓存的机构名称
            if (old.OrgName != orgDTO.OrgName)
            {
                var users = await this.RedisService.GetAsync<List<SysUserDTO>>(RuYiGlobalConfig.SystemCacheConfig.UserCacheName);
                var orgUsers = users.Where(t => t.OrgId == orgDTO.Id).ToList();

                foreach (var item in orgUsers)
                {
                    //删除旧用户
                    var oldUser = item;
                    users.Remove(oldUser);

                    //添加新用户
                    item.OrgName = orgDTO.OrgName;
                    users.Add(item);
                }

                //更新用户缓存
                await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.UserCacheName, users, -1);
            }

            #endregion

            return Ok(actionResponseResult);
        }

        #endregion

        #region 删除机构信息

        /// <summary>
        /// 删除机构信息
        /// </summary>
        /// <param name="orgId">对象编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpDelete("{orgId}")]
        [Log(OperationType.DeleteEntity)]
        [Permission("org:del:entities")]
        public async Task<IActionResult> Delete(Guid orgId)
        {
            await DeleteCheck(orgId.ToString());

            var actionResponseResult = await this.OrganizationService.DeleteAsync(orgId);

            //数据一致性维护
            var orgs = await this.RedisService.GetAsync<List<SysOrganizationDTO>>(RuYiGlobalConfig.SystemCacheConfig.OrgCacheName);
            var org = orgs.Where(t => t.Id == orgId).FirstOrDefault();
            orgs.Remove(org);
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.OrgCacheName, orgs, -1);

            return Ok(actionResponseResult);
        }

        #endregion

        #region 批量删除机构

        /// <summary>
        /// 批量删除机构
        /// </summary>
        /// <param name="ids">编号数组</param>
        /// <returns>ActionResponseResult</returns>
        [HttpDelete("{ids}")]
        [Log(OperationType.DeleteEntity)]
        [Permission("org:del:entities")]
        public async Task<IActionResult> DeleteRange(string ids)
        {
            await DeleteCheck(ids);

            var array = RuYiStringUtil.GetGuids(ids);
            var actionResponseResult = await this.OrganizationService.DeleteRangeAsync(array);

            //数据一致性维护
            var orgs = await this.RedisService.GetAsync<List<SysOrganizationDTO>>(RuYiGlobalConfig.SystemCacheConfig.OrgCacheName);
            foreach (var item in array)
            {
                var org = orgs.Where(t => t.Id == item).FirstOrDefault();
                orgs.Remove(org);
            }
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.OrgCacheName, orgs, -1);

            return Ok(actionResponseResult);
        }

        #endregion

        #region 获取机构用户树

        /// <summary>
        /// 获取机构用户树
        /// </summary>
        /// <returns>QueryResponseResult</returns>
        [HttpPost]
        [Log(OperationType.QueryList)]
        //[Permission("role:query:list,user:query:list")]
        public async Task<IActionResult> GetOrgUserTree()
        {
            var actionResponseResult = await this.OrganizationService.GetOrgUserTree();
            return Ok(actionResponseResult);
        }

        #endregion

        #region 机构删除检测

        /// <summary>
        /// 机构删除检测
        /// </summary>
        /// <param name="ids">编号数组</param>
        /// <returns></returns>
        private async Task DeleteCheck(string ids)
        {
            var users = await this.RedisService.GetAsync<List<SysUserDTO>>(RuYiGlobalConfig.SystemCacheConfig.UserCacheName);
            var orgs = await this.RedisService.GetAsync<List<SysOrganizationDTO>>(RuYiGlobalConfig.SystemCacheConfig.OrgCacheName);

            var array = RuYiStringUtil.GetGuids(ids);
            foreach (var item in array)
            {
                var userCount = users.Where(t => t.OrgId == item).Count();
                var orgCount = orgs.Where(t => t.ParentId == item).Count();

                if (userCount > 0 || orgCount > 0)
                {
                    throw new RuYiAdminCustomException(ExceptionMessage.DeleteOrgExceptionMessage);
                }
            }
        }

        #endregion
    }
}
