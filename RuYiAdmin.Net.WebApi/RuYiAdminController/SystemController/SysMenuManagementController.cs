﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.AspNetCore.Mvc;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Constants.Framework;
using RuYiAdmin.Net.CommonInfrastructure.Constants.System;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.CommonInfrastructure.Exceptions.Framework;
using RuYiAdmin.Net.CommonInfrastructure.Extensions.Business;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Utils;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.FrameworkInterface;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminAnnotation;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RuYiAdmin.Net.WebApi.RuYiAdminController.SystemController
{
    /// <summary>
    /// 菜单管理控制器
    /// </summary>
    public class SysMenuManagementController : RuYiAdminBaseController<SysMenu>
    {
        #region 属性及构造函数

        /// <summary>
        /// 菜单服务实例
        /// </summary>
        private readonly ISysMenuService MenuService;

        /// <summary>
        /// 菜单多语实例
        /// </summary>
        private readonly ISysMenuLanguageService MenuLanguageService;

        /// <summary>
        /// Redis接口实例
        /// </summary>
        private readonly IRedisService RedisService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="MenuService"></param>
        /// <param name="MenuLanguageService"></param>
        /// <param name="RedisService"></param>
        public SysMenuManagementController(ISysMenuService MenuService,
                                        ISysMenuLanguageService MenuLanguageService,
                                        IRedisService RedisService) : base(MenuService)
        {
            this.MenuService = MenuService;
            this.MenuLanguageService = MenuLanguageService;
            this.RedisService = RedisService;
        }

        #endregion

        #region 查询菜单列表

        /// <summary>
        /// 查询菜单列表
        /// </summary>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.QueryList)]
        [Permission("menu:query:list")]
        public async Task<IActionResult> Post()
        {
            var actionResponseResult = await this.MenuService.GetMenuTreeNodes();
            return Ok(actionResponseResult);
        }

        #endregion

        #region 获取菜单信息

        /// <summary>
        /// 获取菜单信息
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpGet("{id}")]
        [Log(OperationType.QueryEntity)]
        [Permission("menu:query:list")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");

            var menus = await this.RedisService.GetAsync<List<SysMenuDTO>>(RuYiGlobalConfig.SystemCacheConfig.MenuCacheName);
            var menu = menus.Where(t => t.Id == id).FirstOrDefault();

            #region 初始化菜单多语

            var languages = this.RedisService.Get<List<SysLanguage>>(RuYiGlobalConfig.SystemCacheConfig.LanguageCacheName);
            var lanEn = languages.Where(t => t.LanguageName.Equals(Keywords.LANGUAGE_EN)).FirstOrDefault();
            var lanRu = languages.Where(t => t.LanguageName.Equals(Keywords.LANGUAGE_RU)).FirstOrDefault();

            var menuLanguages = this.RedisService.Get<List<SysMenuLanguage>>(RuYiGlobalConfig.SystemCacheConfig.MenuAndLanguageCacheName);

            #region 初始化多语

            var enMenu = menuLanguages.Where(t => t.MenuId.Equals(menu.Id) && t.LanguageId.Equals(lanEn.Id)).FirstOrDefault();
            var ruMenu = menuLanguages.Where(t => t.MenuId.Equals(menu.Id) && t.LanguageId.Equals(lanRu.Id)).FirstOrDefault();

            if (enMenu != null)
            {
                menu.MenuNameEn = enMenu.MenuName;
            }

            if (ruMenu != null)
            {
                menu.MenuNameRu = ruMenu.MenuName;
            }

            #endregion

            #endregion

            actionResponseResult.Object = menu;

            return Ok(actionResponseResult);
        }

        #endregion

        #region 新增菜单信息

        /// <summary>
        /// 新增菜单信息
        /// </summary>
        /// <param name="sysMenu">菜单对象</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.AddEntity)]
        [Permission("menu:add:entity")]
        public async Task<IActionResult> Add([FromBody] SysMenuDTO sysMenu)
        {
            var actionResponseResult = await this.MenuService.AddAsync(sysMenu);

            #region 维护菜单多语

            var languages = await this.RedisService.GetAsync<List<SysLanguage>>(RuYiGlobalConfig.SystemCacheConfig.LanguageCacheName);
            var menuLanguages = await this.RedisService.GetAsync<List<SysMenuLanguage>>(RuYiGlobalConfig.SystemCacheConfig.MenuAndLanguageCacheName);

            if (!string.IsNullOrEmpty(sysMenu.MenuNameEn))
            {
                var language = languages.Where(t => t.LanguageName.Equals(Keywords.LANGUAGE_EN)).FirstOrDefault();

                var menuLan = new SysMenuLanguage();
                menuLan.LanguageId = language.Id;
                menuLan.MenuId = sysMenu.Id;
                menuLan.MenuName = sysMenu.MenuNameEn;
                await this.MenuLanguageService.AddAsync(menuLan);

                menuLanguages.Add(menuLan);
            }

            if (!string.IsNullOrEmpty(sysMenu.MenuNameRu))
            {
                var language = languages.Where(t => t.LanguageName.Equals(Keywords.LANGUAGE_RU)).FirstOrDefault();

                var menuLan = new SysMenuLanguage();
                menuLan.LanguageId = language.Id;
                menuLan.MenuId = sysMenu.Id;
                menuLan.MenuName = sysMenu.MenuNameRu;
                await this.MenuLanguageService.AddAsync(menuLan);

                menuLanguages.Add(menuLan);
            }

            #endregion

            #region 数据一致性维护

            var menus = await this.RedisService.GetAsync<List<SysMenuDTO>>(RuYiGlobalConfig.SystemCacheConfig.MenuCacheName);
            var menu = sysMenu;
            if (menu.Children != null)
            {
                menu.Children.Clear();
            }
            menus.Add(menu);
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.MenuCacheName, menus, -1);

            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.MenuAndLanguageCacheName, menuLanguages, -1);

            #endregion

            return Ok(actionResponseResult);
        }

        #endregion

        #region 修改菜单信息

        /// <summary>
        /// 修改菜单信息
        /// </summary>
        /// <param name="sysMenu">菜单对象</param>
        /// <returns>IActionResult</returns>
        [HttpPut]
        [Log(OperationType.EditEntity)]
        [Permission("menu:edit:entity")]
        public async Task<IActionResult> Put([FromBody] SysMenuDTO sysMenu)
        {
            var actionResponseResult = await this.MenuService.UpdateAsync(sysMenu);

            #region 维护菜单多语

            var languages = await this.RedisService.GetAsync<List<SysLanguage>>(RuYiGlobalConfig.SystemCacheConfig.LanguageCacheName);
            var menuLanguages = await this.RedisService.GetAsync<List<SysMenuLanguage>>(RuYiGlobalConfig.SystemCacheConfig.MenuAndLanguageCacheName);

            if (!string.IsNullOrEmpty(sysMenu.MenuNameEn))
            {
                var language = languages.Where(t => t.LanguageName.Equals(Keywords.LANGUAGE_EN)).FirstOrDefault();

                var oldMenuLan = menuLanguages.Where(t => t.LanguageId.Equals(language.Id) && t.MenuId.Equals(sysMenu.Id) && t.IsDel == (int)DeletionType.Undeleted).
                                               FirstOrDefault();

                if (oldMenuLan != null && oldMenuLan.MenuName != sysMenu.MenuNameEn)
                {
                    await this.MenuLanguageService.DeleteAsync(oldMenuLan.Id);

                    menuLanguages.Remove(oldMenuLan);

                    var menuLan = new SysMenuLanguage();
                    menuLan.LanguageId = language.Id;
                    menuLan.MenuId = sysMenu.Id;
                    menuLan.MenuName = sysMenu.MenuNameEn;
                    await this.MenuLanguageService.AddAsync(menuLan);

                    menuLanguages.Add(menuLan);
                }
            }
            if (!string.IsNullOrEmpty(sysMenu.MenuNameRu))
            {
                var language = languages.Where(t => t.LanguageName.Equals(Keywords.LANGUAGE_RU)).FirstOrDefault();

                var oldMenuLan = menuLanguages.Where(t => t.LanguageId.Equals(language.Id) && t.MenuId.Equals(sysMenu.Id) && t.IsDel == (int)DeletionType.Undeleted).
                                               FirstOrDefault();

                if (oldMenuLan != null && oldMenuLan.MenuName != sysMenu.MenuNameRu)
                {
                    await this.MenuLanguageService.DeleteAsync(oldMenuLan.Id);

                    menuLanguages.Remove(oldMenuLan);

                    var menuLan = new SysMenuLanguage();
                    menuLan.LanguageId = language.Id;
                    menuLan.MenuId = sysMenu.Id;
                    menuLan.MenuName = sysMenu.MenuNameRu;
                    await this.MenuLanguageService.AddAsync(menuLan);

                    menuLanguages.Add(menuLan);
                }
            }

            #endregion

            #region 数据一致性维护

            var menus = await this.RedisService.GetAsync<List<SysMenuDTO>>(RuYiGlobalConfig.SystemCacheConfig.MenuCacheName);
            var menu = menus.Where(t => t.Id == sysMenu.Id).FirstOrDefault();
            menus.Remove(menu);
            menu = sysMenu;
            if (menu.Children != null)
            {
                menu.Children.Clear();
            }
            menus.Add(menu);
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.MenuCacheName, menus, -1);

            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.MenuAndLanguageCacheName, menuLanguages, -1);

            #endregion

            return Ok(actionResponseResult);
        }

        #endregion

        #region 删除菜单信息

        /// <summary>
        /// 删除菜单信息
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpDelete("{id}")]
        [Log(OperationType.DeleteEntity)]
        [Permission("menu:del:entities")]
        public async Task<IActionResult> Delete(Guid id)
        {
            //数据删除检测
            await DeleteCheck(id.ToString());

            //数据删除
            var actionResponseResult = await this.MenuService.DeleteAsync(id);

            var menuLanguages = await this.RedisService.GetAsync<List<SysMenuLanguage>>(RuYiGlobalConfig.SystemCacheConfig.MenuAndLanguageCacheName);
            var oldMenuLans = menuLanguages.Where(t => t.MenuId.Equals(id) && t.IsDel == (int)DeletionType.Undeleted).ToList();
            if (oldMenuLans.Count() > 0)
            {
                await this.MenuLanguageService.DeleteRangeAsync(oldMenuLans.Select(t => t.Id).ToArray());

                menuLanguages.RemoveRange(oldMenuLans);
            }

            #region 数据一致性维护

            var menus = await this.RedisService.GetAsync<List<SysMenuDTO>>(RuYiGlobalConfig.SystemCacheConfig.MenuCacheName);
            var menu = menus.Where(t => t.Id == id).FirstOrDefault();
            menus.Remove(menu);
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.MenuCacheName, menus, -1);

            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.MenuAndLanguageCacheName, menuLanguages, -1);

            #endregion

            return Ok(actionResponseResult);
        }

        #endregion

        #region 批量删除菜单信息

        /// <summary>
        /// 批量删除菜单信息
        /// </summary>
        /// <param name="ids">数组串</param>
        /// <returns>ActionResponseResult</returns>
        [HttpDelete("{ids}")]
        [Log(OperationType.DeleteEntity)]
        [Permission("menu:del:entities")]
        public async Task<IActionResult> DeleteRange(string ids)
        {
            //数据删除检测
            await DeleteCheck(ids);

            //数据删除
            var array = RuYiStringUtil.GetGuids(ids);
            var actionResponseResult = await this.MenuService.DeleteRangeAsync(array);

            var menuLanguages = await this.RedisService.GetAsync<List<SysMenuLanguage>>(RuYiGlobalConfig.SystemCacheConfig.MenuAndLanguageCacheName);
            foreach (var item in array)
            {
                var oldMenuLans = menuLanguages.Where(t => t.MenuId.Equals(item) && t.IsDel == (int)DeletionType.Undeleted).ToList();
                if (oldMenuLans.Count > 0)
                {
                    await this.MenuLanguageService.DeleteRangeAsync(oldMenuLans.Select(t => t.Id).ToArray());

                    menuLanguages.RemoveRange(oldMenuLans);
                }
            }

            #region 数据一致性维护

            var menus = await this.RedisService.GetAsync<List<SysMenuDTO>>(RuYiGlobalConfig.SystemCacheConfig.MenuCacheName);
            foreach (var item in array)
            {
                var menu = menus.Where(t => t.Id == item).FirstOrDefault();
                menus.Remove(menu);
            }
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.MenuCacheName, menus, -1);

            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.MenuAndLanguageCacheName, menuLanguages, -1);

            #endregion

            return Ok(actionResponseResult);
        }

        #endregion

        #region 数据删除检测

        /// <summary>
        /// 数据删除检测
        /// </summary>
        /// <param name="ids">数组串</param>
        /// <returns>真假值</returns>
        private async Task DeleteCheck(string ids)
        {
            var menus = await this.RedisService.GetAsync<List<SysMenuDTO>>(RuYiGlobalConfig.SystemCacheConfig.MenuCacheName);

            var array = RuYiStringUtil.GetGuids(ids);
            foreach (var item in array)
            {
                var subMenus = menus.Where(m => m.ParentId.Equals(item)).ToList();
                if (subMenus.Count > 0)
                {
                    throw new RuYiAdminCustomException(ExceptionMessage.DeleteMenuExceptionMessage);
                }
            }
        }

        #endregion
    }
}
