﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Extensions.Business;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.FrameworkInterface;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RuYiAdmin.Net.WebApi.RuYiAdminController.SystemController
{
    /// <summary>
    /// 多语管理控制器
    /// </summary>
    public class SysLanguageManagementController : RuYiAdminBaseController<SysLanguage>
    {
        #region 属性及构造函数

        /// <summary>
        /// 多语管理业务接口实例
        /// </summary>
        private readonly ISysLanguageService LanguageService;

        /// <summary>
        /// Redis接口实例
        /// </summary>
        private readonly IRedisService RedisService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="LanguageService"></param>
        /// <param name="RedisService"></param>
        public SysLanguageManagementController(ISysLanguageService LanguageService,
                                               IRedisService RedisService) : base(LanguageService)
        {
            this.LanguageService = LanguageService;
            this.RedisService = RedisService;
        }

        #endregion

        #region 查询多语列表

        /// <summary>
        /// 查询多语列表
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        //[Log(OperationType.QueryList)]
        //[Permission("language:query:list")]
        [AllowAnonymous]
        public async Task<IActionResult> Post(SearchCondition searchCondition)
        {
            var languages = await this.RedisService.GetAsync<List<SysLanguage>>(RuYiGlobalConfig.SystemCacheConfig.LanguageCacheName);
            languages = languages.AsQueryable().Where(searchCondition.ConvertToLamdaExpression<SysLanguage>()).ToList();

            if (!string.IsNullOrEmpty(searchCondition.Sort))
            {
                languages = languages.Sort(searchCondition.Sort);
            }

            int count = languages.Count;
            languages = languages.Skip(searchCondition.PageIndex * searchCondition.PageSize).Take(searchCondition.PageSize).ToList();

            var actionResponseResult = QueryResponseResult<SysLanguage>.Success(count, languages);
            return Ok(actionResponseResult);
        }

        #endregion
    }
}
