﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Contexts;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Utils;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminAnnotation;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminFilter;

namespace RuYiAdmin.Net.WebApi.RuYiAdminController.SystemController
{
    [Authorize]
    [ActionAuthorization]
    [ApiController]
    [Route(RuYiGlobalConfig.RouteTemplate)]
    public class SysServerHardwareMonitorController : ControllerBase
    {
        #region 获取服务器信息

        /// <summary>
        /// 获取服务器信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Log(OperationType.QueryEntity)]
        [Permission("query:hardware:info")]
        public IActionResult Get()
        {
            var work = RuYiSmartThreadPoolContext.Instance.QueueWorkItem(RuYiHardwareMonitorUtil.StartMonitoring);
            var actionResponseResult = ActionResponseResult.Success(work.Result);
            return Ok(actionResponseResult);
        }

        #endregion
    }
}
