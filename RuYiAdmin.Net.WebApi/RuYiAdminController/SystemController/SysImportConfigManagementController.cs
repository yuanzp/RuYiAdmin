﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.AspNetCore.Mvc;
using RuYiAdmin.Net.CommonInfrastructure.Constants.System;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Framework;
using RuYiAdmin.Net.CommonInfrastructure.Exceptions.Framework;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Utils;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminAnnotation;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RuYiAdmin.Net.WebApi.RuYiAdminController.SystemController
{
    /// <summary>
    /// 导入配置管理控制器
    /// </summary>
    public class SysImportConfigManagementController : RuYiAdminBaseController<SysImportConfig>
    {
        #region 属性及构造函数

        /// <summary>
        /// 导入配置接口实例
        /// </summary>
        private readonly ISysImportConfigService ImportConfigService;

        /// <summary>
        /// 导入配置明细接口实例
        /// </summary>
        private readonly ISysImportConfigDetailService ImportConfigDetailService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="ImportConfigService"></param>
        /// <param name="ImportConfigDetailService"></param>
        public SysImportConfigManagementController(ISysImportConfigService ImportConfigService,
                                                   ISysImportConfigDetailService ImportConfigDetailService
                                                   ) : base(ImportConfigService)
        {
            this.ImportConfigService = ImportConfigService;
            this.ImportConfigDetailService = ImportConfigDetailService;
        }

        #endregion

        #region 查询导入配置列表

        /// <summary>
        /// 查询导入配置列表
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.QueryList)]
        [Permission("import:config:query:list")]
        public async Task<IActionResult> Post(SearchCondition searchCondition)
        {
            var actionResponseResult = await this.ImportConfigService.DefaultSqlQueryAsync(searchCondition);
            return Ok(actionResponseResult);
        }

        #endregion

        #region 按编号获取配置

        /// <summary>
        /// 按编号获取配置
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpGet("{id}")]
        [Log(OperationType.QueryEntity)]
        [Permission("import:config:query:list")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var actionResponseResult = await this.ImportConfigService.GetByIdAsync(id);
            return Ok(actionResponseResult);
        }

        #endregion

        #region 按父键获取配置明细

        /// <summary>
        /// 按父键获取配置明细
        /// </summary>
        /// <param name="parentId">父键</param>
        /// <returns>ActionResponseResult</returns>
        [HttpGet("{parentId}")]
        [Log(OperationType.QueryList)]
        [Permission("import:config:query:list")]
        public async Task<IActionResult> GetByParentId(Guid parentId)
        {
            var task = await this.ImportConfigDetailService.GetListAsync();
            var list = (List<SysImportConfigDetail>)task.Object;
            list = list.Where(t => t.IsDel == (int)DeletionType.Undeleted).OrderByDescending(t => t.CreateTime).ToList();

            var actionResponseResult = ActionResponseResult.OK(list.Where(t => t.ParentId.Equals(parentId)).OrderBy(t => t.SerialNumber).ToList());
            return Ok(actionResponseResult);
        }

        #endregion

        #region 添加导入配置

        /// <summary>
        /// 添加导入配置
        /// </summary>
        /// <param name="importConfig">导入配置</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.AddEntity)]
        [Permission("import:config:add:entity")]
        public async Task<IActionResult> AddConfig([FromBody] SysImportConfig importConfig)
        {
            var actionResponseResult = await this.ImportConfigService.AddAsync(importConfig);
            return Ok(actionResponseResult);
        }

        #endregion

        #region 添加导入配置明细

        /// <summary>
        ///添加导入配置明细
        /// </summary>
        /// <param name="configDetail">配置明细</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.AddEntity)]
        [Permission("import:config:add:entity")]
        public async Task<IActionResult> AddConfigDetail([FromBody] SysImportConfigDetail configDetail)
        {
            var actionResponseResult = await this.ImportConfigDetailService.AddAsync(configDetail);
            return Ok(actionResponseResult);
        }

        #endregion

        #region 编辑导入配置

        /// <summary>
        /// 编辑导入配置
        /// </summary>
        /// <param name="importConfig">导入配置</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPut]
        [Log(OperationType.EditEntity)]
        [Permission("import:config:edit:entity")]
        public async Task<IActionResult> EditConfig([FromBody] SysImportConfig importConfig)
        {
            var actionResponseResult = await this.ImportConfigService.UpdateAsync(importConfig);
            return Ok(actionResponseResult);
        }

        #endregion

        #region 编辑导入配置明细

        /// <summary>
        ///编辑导入配置明细
        /// </summary>
        /// <param name="configDetail">配置明细</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPut]
        [Log(OperationType.EditEntity)]
        [Permission("import:config:edit:entity")]
        public async Task<IActionResult> EditConfigDetail([FromBody] SysImportConfigDetail configDetail)
        {
            var actionResponseResult = await this.ImportConfigDetailService.UpdateAsync(configDetail);
            return Ok(actionResponseResult);
        }

        #endregion

        #region 删除导入配置

        /// <summary>
        /// 删除导入配置
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpDelete("{id}")]
        [Log(OperationType.DeleteEntity)]
        [Permission("import:config:del:entities")]
        public async Task<IActionResult> DeleteConfig(Guid id)
        {
            //数据删除检测
            await DeleteCheck(id.ToString());
            //数据删除
            var actionResponseResult = await this.ImportConfigService.DeleteAsync(id);
            return Ok(actionResponseResult);
        }

        #endregion

        #region 批量删除导入配置

        /// <summary>
        /// 批量删除导入配置
        /// </summary>
        /// <param name="ids">数组串</param>
        /// <returns>ActionResponseResult</returns>
        [HttpDelete("{ids}")]
        [Log(OperationType.DeleteEntity)]
        [Permission("import:config:del:entities")]
        public async Task<IActionResult> DeleteConfigs(string ids)
        {
            //数据删除检测
            await DeleteCheck(ids);
            //数据删除
            var array = RuYiStringUtil.GetGuids(ids);
            var actionResponseResult = await this.ImportConfigService.DeleteRangeAsync(array);
            return Ok(actionResponseResult);
        }

        #endregion

        #region 删除配置明细

        /// <summary>
        /// 删除配置明细
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpDelete("{id}")]
        [Log(OperationType.DeleteEntity)]
        [Permission("import:config:del:entities")]
        public async Task<IActionResult> DeleteConfigDetail(Guid id)
        {
            var actionResponseResult = await this.ImportConfigDetailService.DeleteAsync(id);
            return Ok(actionResponseResult);
        }

        #endregion

        #region 批量删除配置明细

        /// <summary>
        /// 批量删除配置明细
        /// </summary>
        /// <param name="ids">数组串</param>
        /// <returns>ActionResponseResult</returns>
        [HttpDelete("{ids}")]
        [Log(OperationType.DeleteEntity)]
        [Permission("import:config:del:entities")]
        public async Task<IActionResult> DeleteConfigDetails(string ids)
        {
            var array = RuYiStringUtil.GetGuids(ids);
            var actionResponseResult = await this.ImportConfigDetailService.DeleteRangeAsync(array);
            return Ok(actionResponseResult);
        }

        #endregion

        #region 删除子表检测

        /// <summary>
        /// 删除子表检测
        /// </summary>
        /// <param name="ids">数组串</param>
        /// <returns>真假值</returns>
        private async Task DeleteCheck(string ids)
        {
            var array = RuYiStringUtil.GetGuids(ids);
            foreach (var item in array)
            {
                var searchCondition = new SearchCondition();
                searchCondition.SearchItems = new List<SearchItem>();
                searchCondition.SearchItems.Add(new SearchItem()
                {
                    Field = "ParentId",
                    DataType = DataType.Guid,
                    SearchMethod = SearchMethod.Equal,
                    Value = item
                });

                var data = await this.ImportConfigDetailService.GetListAsync(searchCondition);
                if (data.List.Count > 0)
                {
                    throw new RuYiAdminCustomException(ExceptionMessage.DeleteImportConfigExceptionMessage);
                }
            }
        }

        #endregion
    }
}
