﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

namespace RuYiAdmin.Net.CommonInfrastructure.Enums.Business
{
    /// <summary>
    /// YesNo枚举
    /// </summary>
    public enum YesNo
    {
        /// <summary>
        /// NO
        /// </summary>
        NO,

        /// <summary>
        /// YES
        /// </summary>
        YES
    }
}
