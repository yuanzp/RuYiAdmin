﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Enums.Framework;
using System;

namespace RuYiAdmin.Net.CommonInfrastructure.Models
{
    /// <summary>
    /// 查询项
    /// </summary>
    public class SearchItem
    {
        /// <summary>
        /// 查询项字段
        /// </summary>
        public String Field { get; set; }

        /// <summary>
        /// 查询项类型
        /// </summary>
        public DataType DataType { get; set; }

        /// <summary>
        /// 查询项方法
        /// </summary>
        public SearchMethod SearchMethod { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public Object Value { get; set; }

        /// <summary>
        /// 无参构造函数
        /// </summary>
        public SearchItem()
        {

        }

        /// <summary>
        /// 有参构造函数
        /// </summary>
        /// <param name="field">字段</param>
        /// <param name="dataType">数据类型</param>
        /// <param name="searchMethod">查询方法</param>
        /// <param name="value">值</param>
        public SearchItem(String field, DataType dataType, SearchMethod searchMethod, Object value)
        {
            Field = field;
            DataType = dataType;
            SearchMethod = searchMethod;
            Value = value;
        }

        /// <summary>
        /// 获取缺省QueryItem
        /// </summary>
        /// <returns>QueryItem</returns>
        public static SearchItem GetDefault()
        {
            return new SearchItem()
            {
                Field = "IsDel",
                DataType = DataType.Int,
                SearchMethod = SearchMethod.Equal,
                Value = 0
            };
        }
    }
}
