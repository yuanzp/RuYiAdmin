﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

namespace RuYiAdmin.Net.CommonInfrastructure.Configurations
{
    /// <summary>
    /// Apollo客户端配置
    /// </summary>
    public class ApolloConfig
    {
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// AppId
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// MetaServer
        /// </summary>
        public string MetaServer { get; set; }

        /// <summary>
        /// ConfigServer
        /// </summary>
        public string[] ConfigServer { get; set; }

        /// <summary>
        /// Env
        /// </summary>
        public string Env { get; set; }
    }
}
