﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

namespace RuYiAdmin.Net.CommonInfrastructure.Configurations
{
    /// <summary>
    /// Polly配置
    /// </summary>
    public class PollyConfig
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 是否启用熔断降级
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// 超时时间
        /// </summary>
        public int Timeout { get; set; }

        /// <summary>
        /// 重试次数
        /// </summary>
        public int RetryCount { get; set; }

        /// <summary>
        /// 熔断开启次数
        /// </summary>
        public int OpenFallCount { get; set; }

        /// <summary>
        /// 熔断时间
        /// </summary>
        public int DownTime { get; set; }

        /// <summary>
        /// 降级信息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 运维邮箱
        /// </summary>
        public string OMMailbox { get; set; }
    }
}
