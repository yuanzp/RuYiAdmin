﻿namespace RuYiAdmin.Net.CommonInfrastructure.Configurations
{
    /// <summary>
    /// MiniProfiler配置
    /// </summary>
    public class MiniProfilerConfig
    {
        /// <summary>
        /// profiler URL
        /// </summary>
        public string RouteBasePath { get; set; }

        /// <summary>
        /// CacheDuration
        /// </summary>
        public int CacheDuration { get; set; }
    }
}
