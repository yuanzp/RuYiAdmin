﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

namespace RuYiAdmin.Net.CommonInfrastructure.Configurations
{
    /// <summary>
    /// 系统缓存配置
    /// </summary>
    public class SystemCacheConfig
    {
        /// <summary>
        /// 机构缓存名称
        /// </summary>
        public string OrgCacheName { get; set; }

        /// <summary>
        /// 用户缓存名称
        /// </summary>
        public string UserCacheName { get; set; }

        /// <summary>
        /// 菜单缓存名称
        /// </summary>
        public string MenuCacheName { get; set; }

        /// <summary>
        /// 菜单与多语缓存名称
        /// </summary>
        public string MenuAndLanguageCacheName { get; set; }

        /// <summary>
        /// 角色缓存名称
        /// </summary>
        public string RoleCacheName { get; set; }

        /// <summary>
        /// 角色菜单缓存名称
        /// </summary>
        public string RoleAndMenuCacheName { get; set; }

        /// <summary>
        /// 角色机构缓存名称
        /// </summary>
        public string RoleAndOrgCacheName { get; set; }

        /// <summary>
        /// 角色用户缓存名称
        /// </summary>
        public string RoleAndUserCacheName { get; set; }

        /// <summary>
        /// 数据字典缓存名称
        /// </summary>
        public string CodeTableCacheName { get; set; }

        /// <summary>
        /// 多语缓存名称
        /// </summary>
        public string LanguageCacheName { get; set; }

        /// <summary>
        /// 计划业务缓存名称
        /// </summary>
        public string ScheduleJobCacheName { get; set; }

        /// <summary>
        /// 行政区域缓存名称
        /// </summary>
        public string AreaCacheName { get; set; }
    }
}
