﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;

namespace RuYiAdmin.Net.CommonInfrastructure.Configurations
{
    /// <summary>
    /// 消息中间件配置
    /// </summary>
    public class MomConfig
    {
        /// <summary>
        /// 消息中间件类型
        /// </summary>
        public MomType MomType { get; set; }
    }
}
