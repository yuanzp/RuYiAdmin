﻿namespace RuYiAdmin.Net.CommonInfrastructure.Configurations
{
    /// <summary>
    /// Meilisearch配置
    /// </summary>
    public class MeilisearchConfig
    {
        /// <summary>
        /// URL
        /// </summary>
        public string URL { get; set; }

        /// <summary>
        /// ApiKey
        /// </summary>
        public string ApiKey { get; set; }

        /// <summary>
        /// Index
        /// </summary>
        public string Index { get; set; }
    }
}
