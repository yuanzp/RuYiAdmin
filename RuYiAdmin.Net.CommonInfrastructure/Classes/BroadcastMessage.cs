﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using System;

namespace RuYiAdmin.Net.CommonInfrastructure.Classes
{
    /// <summary>
    /// 系统广播消息
    /// </summary>
    public class BroadcastMessage
    {
        /// <summary>
        /// 标题
        /// </summary>
        public String Title { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public String Message { get; set; }

        /// <summary>
        /// 消息级别
        /// </summary>
        public MessageLevel MessageLevel { get; set; }
    }
}
