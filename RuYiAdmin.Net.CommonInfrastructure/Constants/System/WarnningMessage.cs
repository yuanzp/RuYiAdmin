using System;

namespace RuYiAdmin.Net.CommonInfrastructure.Constants.System
{
    /// <summary>
    /// 系统警告提示信息
    /// </summary>
    public class WarnningMessage
    {
        /// <summary>
        /// 配置文件未找到警告提示信息
        /// </summary>
        public const String ImportConfigNotFindMessage = "import configuration can not be found";

        /// <summary>
        /// 未找到警告提示信息
        /// </summary>
        public const String NotFindMessage = "not found";

        /// <summary>
        /// 无内容警告提示信息
        /// </summary>
        public const String NoContentMessage = "no content";

        /// <summary>
        /// 禁止警告提示信息
        /// </summary>
        public const String ForbiddenMessage = "forbidden";

        /// <summary>
        /// 关系已存在警告提示信息
        /// </summary>
        public const String DuplicatedRelationshipMessage = "relationship has existed";

        /// <summary>
        /// Sql注入式攻击警告提示信息
        /// </summary>
        public const String SqlInjectionAttackMessage = "sql injection attack";

        /// <summary>
        /// 拒绝访问警告提示信息
        /// </summary>
        public const String AccessDeniedMessage = "access denied";

        /// <summary>
        /// 非法访问警告提示信息
        /// </summary>
        public const String IllegalAccessMessage = "illegal access";

        /// <summary>
        /// 必要项警告提示信息
        /// </summary>
        public const String NeccesarryItemMessage = " is necessary";
    }
}
