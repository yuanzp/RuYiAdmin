﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.AspNetCore.Http;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Constants.Framework;

namespace RuYiAdmin.Net.CommonInfrastructure.Utilities.Utils
{
    /// <summary>
    /// Token工具类
    /// </summary>
    public static class RuYiTokenUtil
    {
        /// <summary>
        /// 获取token
        /// </summary>
        /// <param name="context">Http会话</param>
        /// <returns>token</returns>
        public static string GetToken(this HttpContext context)
        {
            var token = string.Empty;

            //从头部获取token
            token = context.Request.Headers[Keywords.TOKEN];

            //token解密
            token = RuYiRsaUtil.PemDecrypt(token, RuYiGlobalConfig.SystemConfig.RsaPrivateKey);
            //移除salt
            token = token.Split('^')[0];

            //返回token
            return token;
        }

        /// <summary>
        /// 获取token salt
        /// </summary>
        /// <param name="context">Http会话</param>
        /// <returns>TokenSalt</returns>
        public static string GetTokenSalt(this HttpContext context)
        {
            var tokenSalt = string.Empty;

            //从头部获取token
            tokenSalt = context.Request.Headers[Keywords.TOKEN];

            //token解密
            tokenSalt = RuYiRsaUtil.PemDecrypt(tokenSalt, RuYiGlobalConfig.SystemConfig.RsaPrivateKey);
            //移除salt
            tokenSalt = tokenSalt.Split('^')[1];

            //返回token
            return tokenSalt;
        }
    }
}
