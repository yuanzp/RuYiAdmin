﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Apache.NMS;
using Apache.NMS.ActiveMQ;
using Apache.NMS.ActiveMQ.Commands;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Constants.Framework;
using System;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.CommonInfrastructure.Utilities.Contexts
{
    /// <summary>
    /// ActiveMQ工具类
    /// </summary>
    public class RuYiActiveMQContext
    {
        /// <summary>
        /// 发送Topic
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="topicName">topic别名</param>
        /// <returns></returns>
        public static void SendTopic(string message, string topicName = null)
        {
            SendMessage(MQMessageType.TOPIC, message, topicName);
        }

        /// <summary>
        /// 发送Topic
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="topicName">topic别名</param>
        /// <returns></returns>
        public static async Task SendTopicAsync(string message, string topicName = null)
        {
            await SendMessageAsync(MQMessageType.TOPIC, message, topicName);
        }

        /// <summary>
        /// 发送Queue
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="queueName">queue别名</param>
        /// <returns></returns>
        public static void SendQueue(string message, string queueName = null)
        {
            SendMessage(MQMessageType.QUEUE, message, queueName);
        }
        /// <summary>
        /// 发送Queue
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="queueName">queue别名</param>
        /// <returns></returns>
        public static async Task SendQueueAsync(string message, string queueName = null)
        {
            await SendMessageAsync(MQMessageType.QUEUE, message, queueName);
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="type">消息类型</param>
        /// <param name="message">消息</param>
        /// <param name="tqName">别名</param>
        public static void SendMessage(string type, string message, string tqName = null)
        {
            Send(type, message, tqName);
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="type">消息类型</param>
        /// <param name="message">消息</param>
        /// <param name="tqName">别名</param>
        public static async Task SendMessageAsync(string type, string message, string tqName = null)
        {
            await Task.Run(() => { Send(type, message, tqName); });
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="type">消息类型</param>
        /// <param name="message">消息</param>
        /// <param name="tqName">别名</param>
        private static void Send(string type, string message, string tqName = null)
        {
            IConnectionFactory _factory = new ConnectionFactory(new Uri(RuYiGlobalConfig.ActiveMQConfig.ConnectionString));
            using (IConnection _conn = _factory.CreateConnection())
            {
                using (ISession _session = _conn.CreateSession())
                {
                    if (type.Equals(MQMessageType.QUEUE))
                    {
                        var queueName = RuYiGlobalConfig.ActiveMQConfig.QueueName;
                        if (!string.IsNullOrEmpty(tqName))
                        {
                            queueName += tqName;
                        }
                        using (IMessageProducer producer = _session.CreateProducer(new ActiveMQQueue(queueName)))
                        {
                            ITextMessage request = _session.CreateTextMessage(message);
                            producer.Send(request);
                        }
                    }
                    else if (type.Equals(MQMessageType.TOPIC))
                    {
                        var topicName = RuYiGlobalConfig.ActiveMQConfig.TopicName;
                        if (!string.IsNullOrEmpty(tqName))
                        {
                            topicName += tqName;
                        }
                        using (IMessageProducer producer = _session.CreateProducer(new ActiveMQTopic(topicName)))
                        {
                            ITextMessage request = _session.CreateTextMessage(message);
                            producer.Send(request);
                        }
                    }
                }
            }
        }
    }
}
