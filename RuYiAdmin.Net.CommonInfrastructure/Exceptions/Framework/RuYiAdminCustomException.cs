﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using System;
using System.Runtime.Serialization;

namespace RuYiAdmin.Net.CommonInfrastructure.Exceptions.Framework
{
    /// <summary>
    /// 自定义异常
    /// </summary>
    public class RuYiAdminCustomException : Exception
    {
        /// <summary>
        /// 无参构造函数
        /// </summary>
        public RuYiAdminCustomException() : base()
        {
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="info">System.Runtime.Serialization.SerializationInfo</param>
        /// <param name="context">System.Runtime.Serialization.StreamingContext</param>
        protected RuYiAdminCustomException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="message">异常信息</param>
        public RuYiAdminCustomException(string message = null) : base(message)
        {
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="message">异常信息</param>
        /// <param name="innerException">内部异常</param>
        public RuYiAdminCustomException(string message = null, Exception innerException = null) : base(message, innerException)
        {
        }
    }
}
