export function generateSearchItem(field, dataType, searchMethod, value) {
  return {
    'Field': field,
    'DataType': dataType,
    'SearchMethod': searchMethod,
    'Value': value
  }
}
