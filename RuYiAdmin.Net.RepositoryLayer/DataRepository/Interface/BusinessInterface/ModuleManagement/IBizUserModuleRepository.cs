//-----------------------------------------------------------------------
// <Copyright file="IBizUserModuleRepository.cs" company="RuYiAdmin">
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// * Version : 4.0.30319.42000
// * Author  : auto generated by RuYiAdmin T4 Template
// * FileName: IBizUserModuleRepository.cs
// * History : Created by RuYiAdmin 01/21/2022 13:30:46
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.EntityModel.BusinessModel.ModuleManagement;
using RuYiAdmin.Net.RepositoryLayer.BaseRepository.Interface;

namespace RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.BusinessInterface.ModuleManagement
{
    /// <summary>
    /// BizUserModule数据访问层接口
    /// </summary>   
    public interface IBizUserModuleRepository : IRuYiAdminBaseRepository<BizUserModule>
    {
    }
}
