﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Nest;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Contexts;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.FrameworkInterface;

namespace RuYiAdmin.Net.RepositoryLayer.DataRepository.Implementation.FrameworkImplementation
{
    /// <summary>
    /// 
    /// </summary>
    public class NestRepository : INestRepository
    {
        /// <summary>
        /// 获取ElasticClient实例
        /// </summary>
        /// <returns>ElasticClient</returns>
        public ElasticClient GetElasticClient()
        {
            return RuYiEsNestContext.Instance;
        }
    }
}
