﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using MongoDB.Driver;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Contexts;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.FrameworkInterface;

namespace RuYiAdmin.Net.RepositoryLayer.DataRepository.Implementation.FrameworkImplementation
{
    /// <summary>
    /// MongoDB仓储层实现
    /// </summary>
    public class MongoDBRepository : IMongoDBRepository
    {
        /// <summary>
        /// 获取业务集合
        /// </summary>
        /// <typeparam name="TDocument">TDocument</typeparam>
        /// <param name="collectionName">集合名称</param>
        /// <returns>业务集合</returns>
        public IMongoCollection<TDocument> GetBusinessCollection<TDocument>(string collectionName)
        {
            return RuYiMongoDBContext.Instance.GetBusinessCollection<TDocument>(collectionName);
        }

        /// <summary>
        /// 获取MongoClient
        /// </summary>
        /// <returns>MongoClient</returns>
        public MongoClient GetMongoDBClient()
        {
            return RuYiMongoDBContext.Instance;
        }
    }
}
