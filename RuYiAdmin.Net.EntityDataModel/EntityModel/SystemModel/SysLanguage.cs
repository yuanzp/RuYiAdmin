﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.BaseEntityModel;
using SqlSugar;
using System.ComponentModel.DataAnnotations;

namespace RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel
{
    /// <summary>
    /// 多语模型
    /// </summary>
    [SugarTable("sys_language")]
    public class SysLanguage : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 语言名称
        /// </summary>
        [Required, MaxLength(45)]
        [SugarColumn(ColumnName = "LANGUAGE_NAME")]
        public string LanguageName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [SugarColumn(ColumnName = "ORDER_NUMBER")]
        public int? OrderNumber { get; set; }

        /// <summary>
        /// 预留字段1
        /// </summary>
        [SugarColumn(ColumnName = "EXTEND1")]
        public string Extend1 { get; set; }

        /// <summary>
        /// 预留字段2
        /// </summary>
        [SugarColumn(ColumnName = "EXTEND2")]
        public string Extend2 { get; set; }

        /// <summary>
        /// 预留字段3
        /// </summary>
        [SugarColumn(ColumnName = "EXTEND3")]
        public string Extend3 { get; set; }

        /// <summary>
        /// 预留字段4
        /// </summary>
        [SugarColumn(ColumnName = "EXTEND4")]
        public string Extend4 { get; set; }

        /// <summary>
        /// 预留字段5
        /// </summary>
        [SugarColumn(ColumnName = "EXTEND5")]
        public string Extend5 { get; set; }
    }
}
