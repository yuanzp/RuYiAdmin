﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Attributes.Framework.ExcelAttribute;
using RuYiAdmin.Net.EntityDataModel.BaseEntityModel;
using SqlSugar;
using System.ComponentModel.DataAnnotations;

namespace RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel
{
    /// <summary>
    /// 角色模型
    /// </summary>
    [SugarTable("sys_role")]
    public class SysRole : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 角色名称
        /// </summary>
        [Required, MaxLength(128)]
        [SugarColumn(ColumnName = "ROLE_NAME")]
        [ExcelExport("角色名称")]
        [ExcelImport("角色名称")]
        public string RoleName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [ExcelExport("排序")]
        [ExcelImport("排序")]
        [SugarColumn(ColumnName = "SERIAL_NUMBER")]
        public int? SerialNumber { get; set; }

        /// <summary>
        /// 预留字段1
        /// </summary>
        [SugarColumn(ColumnName = "EXTEND1")]
        public string Extend1 { get; set; }

        /// <summary>
        /// 预留字段2
        /// </summary>
        [SugarColumn(ColumnName = "EXTEND2")]
        public string Extend2 { get; set; }

        /// <summary>
        /// 预留字段3
        /// </summary>
        [SugarColumn(ColumnName = "EXTEND3")]
        public string Extend3 { get; set; }

        /// <summary>
        /// 预留字段4
        /// </summary>
        [SugarColumn(ColumnName = "EXTEND4")]
        public string Extend4 { get; set; }

        /// <summary>
        /// 预留字段5
        /// </summary>
        [SugarColumn(ColumnName = "EXTEND5")]
        public string Extend5 { get; set; }
    }
}
