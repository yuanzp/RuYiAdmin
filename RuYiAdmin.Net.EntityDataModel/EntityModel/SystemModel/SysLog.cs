﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Masuit.Tools.Core.Validator;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.EntityDataModel.BaseEntityModel;
using SqlSugar;
using System;
using System.ComponentModel.DataAnnotations;

namespace RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel
{
    /// <summary>
    /// 审计日志模型
    /// </summary>
    [SugarTable("sys_log")]
    public class SysLog : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 用户编号
        /// </summary>
        [Required]
        [SugarColumn(ColumnName = "USER_ID")]
        public Guid UserId { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        [Required, MaxLength(128)]
        [SugarColumn(ColumnName = "USER_NAME")]
        public string UserName { get; set; }

        /// <summary>
        /// 机构编号
        /// </summary>
        [Required]
        [SugarColumn(ColumnName = "ORG_ID")]
        public Guid OrgId { get; set; }

        /// <summary>
        /// 机构名称
        /// </summary>
        [Required, MaxLength(128)]
        [SugarColumn(ColumnName = "ORG_NAME")]
        public string OrgName { get; set; }

        /// <summary>
        /// 使用系统
        /// </summary>
        [SugarColumn(ColumnName = "SYSTEM")]
        public string System { get; set; }

        /// <summary>
        /// 使用浏览器
        /// </summary>
        [SugarColumn(ColumnName = "BROWSER")]
        public string Browser { get; set; }

        /// <summary>
        /// IP地址
        /// </summary>
        [IsIPAddress]
        [SugarColumn(ColumnName = "IP")]
        public string IP { get; set; }

        /// <summary>
        /// 操作类型
        /// </summary>
        [Required]
        [SugarColumn(ColumnName = "OPERATION_TYPE")]
        public OperationType OperationType { get; set; }

        /// <summary>
        /// 请求路径
        /// </summary>
        [Required]
        [SugarColumn(ColumnName = "REQUEST_METHOD")]
        public string RequestMethod { get; set; }

        /// <summary>
        /// 请求路径
        /// </summary>
        [Required]
        [SugarColumn(ColumnName = "REQUEST_URL")]
        public string RequestUrl { get; set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        [SugarColumn(ColumnName = "PARAMS")]
        public string Params { get; set; }

        /// <summary>
        /// 返回值
        /// </summary>
        [SugarColumn(ColumnName = "RESULT")]
        public string Result { get; set; }

        /// <summary>
        /// 记录旧值
        /// </summary>
        [SugarColumn(ColumnName = "OLD_VALUE")]
        public string OldValue { get; set; }

        /// <summary>
        /// 记录新值
        /// </summary>
        [SugarColumn(ColumnName = "NEW_VALUE")]
        public string NewValue { get; set; }
    }
}
