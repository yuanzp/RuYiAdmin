﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.EntityModel.BusinessModel.ModuleManagement;

namespace RuYiAdmin.Net.EntityDataModel.DataTransformationModel.BusinessModel.ModuleManagement
{
    /// <summary>
    /// BizModule DTO
    /// </summary>
    public class BizModuleDTO : BizModule
    {
        /// <summary>
        /// 用户所在模块登录账号
        /// </summary>
        public string UserModuleLogonName { get; set; }

        /// <summary>
        /// 用户所在模块登录密码
        /// </summary>
        public string UserModulePassword { get; set; }
    }
}
