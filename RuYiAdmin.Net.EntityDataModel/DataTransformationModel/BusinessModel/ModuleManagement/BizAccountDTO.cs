﻿using RuYiAdmin.Net.EntityDataModel.EntityModel.BusinessModel.ModuleManagement;

namespace RuYiAdmin.Net.EntityDataModel.DataTransformationModel.BusinessModel.ModuleManagement
{
    /// <summary>
    /// 同步账号DTO
    /// </summary>
    public class BizAccountDTO : BizAccount
    {
        /// <summary>
        /// 口令
        /// </summary>
        public string Token;
    }
}
