﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Attributes.Framework.ExcelAttribute;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using SqlSugar;
using System;
using System.ComponentModel.DataAnnotations;

namespace RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel
{
    /// <summary>
    /// 用户DTO
    /// </summary>
    public class SysUserDTO : SysUser
    {
        /// <summary>
        /// 机构编号
        /// </summary>
        [Required]
        [SugarColumn(ColumnName = "ORG_ID")]
        public Guid OrgId { get; set; }

        /// <summary>
        /// 机构名称
        /// </summary>
        [SugarColumn(ColumnName = "ORG_NAME")]
        [ExcelExport("所在机构")]
        public string OrgName { get; set; }

        /// <summary>
        /// token
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// token有效时间
        /// 单位：秒
        /// </summary>
        public int TokenExpiration { get; set; }
    }
}
