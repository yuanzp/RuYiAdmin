﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using System.Collections.Generic;

namespace RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel
{
    /// <summary>
    /// 机构DTO
    /// </summary>
    public class SysOrganizationDTO : SysOrganization
    {
        /// <summary>
        /// 主管人姓名
        /// </summary>
        public string LeaderName { get; set; }

        /// <summary>
        /// 子集
        /// </summary>
        public List<SysOrganizationDTO> Children { get; set; }
    }
}
